Nginx configuration
-------------------
```
server {
	listen 80;
	root /srv/asker-symfony.local/web;
	server_name asker-symfony.local *.asker-symfony.local;

	location = /favicon.ico { log_not_found off; access_log off; }
	location = /robots.txt { log_not_found off; access_log off; }
	location ~* \.(jpg|jpeg|gif|css|png|js|ico|xml)$ {
		access_log off;
		log_not_found off;
	}

	location / {
	    try_files $uri /app.php$is_args$args;
	}

    # DEV
    location ~ ^/(app_dev|config)\.php(/|$) {
        fastcgi_pass unix:/run/php/php7.0-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
    }
    # PROD
    location ~ ^/app\.php(/|$) {
        fastcgi_pass unix:/run/php/php7.0-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
        internal;
    }

    location ~ \.php$ {
      return 404;
    }

	location ~ /\.ht {
		deny all;
	}
	client_max_body_size 0;
}
```
