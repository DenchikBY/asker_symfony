<?php

namespace AppBundle\Menu;

use AppBundle\Entity\User;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\DependencyInjection\{
    ContainerAwareInterface, ContainerAwareTrait, ContainerInterface
};

/**
 * @property ContainerInterface container
 */
class Main implements ContainerAwareInterface
{

    use ContainerAwareTrait;

    /**
     * @param FactoryInterface $factory
     * @param array $options
     * @return ItemInterface
     */
    public function mainMenu(FactoryInterface $factory, array $options): ItemInterface
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav navbar-right');
        if ($user instanceof User) {
            $menu->addChild('menu.main.feed', ['route' => 'site_account_wall']);
            $menu->addChild('menu.main.inbox', ['route' => 'site_account_inbox']);
            $menu->addChild('menu.main.profile', ['route' => 'site_profile_show', 'routeParameters' => ['username' => $user->getUsername()]]);
            $menu->addChild('menu.main.friends', ['route' => 'site_account_friends']);
            $menu->addChild('menu.main.notifications', ['route' => 'site_account_notifications']);
            $menu->addChild('menu.main.menu')->setAttribute('dropdown', true);
            $menu['menu.main.menu']->addChild('menu.main.settings', ['route' => 'site_account_settings']);
            $menu['menu.main.menu']->addChild('menu.main.logout', ['route' => 'fos_user_security_logout']);
        } else {
            $menu->addChild('menu.main.register', ['route' => 'fos_user_registration_register']);
            $menu->addChild('menu.main.login', ['route' => 'fos_user_security_login']);
        }
        return $menu;
    }

    /**
     * @param FactoryInterface $factory
     * @param array $options
     * @return ItemInterface
     */
    public function desktopMenu(FactoryInterface $factory, array $options): ItemInterface
    {
        /** @var User $user */
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav navbar-right');
        $menu->addChild('menu.main.feed', ['route' => 'site_account_wall']);
        $menu->addChild('menu.main.inbox', ['route' => 'site_account_inbox']);
        $menu->addChild('menu.main.profile', ['route' => 'site_profile_show', 'routeParameters' => ['username' => $user->getUsername()]]);
        $menu->addChild('menu.main.friends', ['route' => 'site_account_friends']);
        $menu->addChild('menu.main.notifications', ['route' => 'site_account_notifications']);
        return $menu;
    }

}
