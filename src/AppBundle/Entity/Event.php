<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="events")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EventRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Event
{

    use BaseEntity;

    const TYPE_ANSWER = 1;
    const TYPE_LIKE = 2;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="user1_id", onDelete="CASCADE")
     * @var User
     */
    protected $eventUser;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="user2_id", onDelete="CASCADE")
     * @var User
     */
    protected $questionReceiverUser;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Question")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @var Question
     */
    protected $question;

    /**
     * @ORM\Column(type="smallint")
     * @var int
     */
    protected $type;

    /**
     * @return User
     */
    public function getEventUser()
    {
        return $this->eventUser;
    }

    /**
     * @param User $eventUser
     * @return $this
     */
    public function setEventUser(User $eventUser)
    {
        $this->eventUser = $eventUser;
        return $this;
    }

    /**
     * @return User
     */
    public function getQuestionReceiverUser()
    {
        return $this->questionReceiverUser;
    }

    /**
     * @param User $questionReceiverUser
     * @return $this
     */
    public function setQuestionReceiverUser(User $questionReceiverUser)
    {
        $this->questionReceiverUser = $questionReceiverUser;
        return $this;
    }

    /**
     * @return Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param Question $question
     * @return $this
     */
    public function setQuestion(Question $question)
    {
        $this->question = $question;
        return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return $this
     */
    public function setType(int $type)
    {
        $this->type = $type;
        return $this;
    }

}
