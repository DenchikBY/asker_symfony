<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="user_socials")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserSocialsRepository")
 */
class UserSocials
{

    /**
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\User", inversedBy="userSocials", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @var User
     */
    protected $user;

    /**
     * @ORM\Column(name="vkontakte_id", type="string", length=255, nullable=true, unique=true)
     * @var string
     */
    protected $vkontakteId;

    /**
     * @ORM\Column(name="vkontakte_access_token", type="string", length=255, nullable=true)
     * @var string
     */
    protected $vkontakteAccessToken;

    /**
     * @ORM\Column(name="facebook_id", type="string", length=255, nullable=true, unique=true)
     * @var string
     */
    protected $facebookId;

    /**
     * @ORM\Column(name="facebook_access_token", type="string", length=255, nullable=true)
     * @var string
     */
    protected $facebookAccessToken;

    /**
     * @ORM\Column(name="twitter_id", type="string", length=255, nullable=true, unique=true)
     * @var string
     */
    protected $twitterId;

    /**
     * @ORM\Column(name="twitter_access_token", type="string", length=255, nullable=true)
     * @var string
     */
    protected $twitterAccessToken;

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getVkontakteId()
    {
        return $this->vkontakteId;
    }

    /**
     * @param string $vkontakteId
     * @return $this
     */
    public function setVkontakteId(string $vkontakteId)
    {
        $this->vkontakteId = $vkontakteId;
        return $this;
    }

    /**
     * @return string
     */
    public function getVkontakteAccessToken()
    {
        return $this->vkontakteAccessToken;
    }

    /**
     * @param string $vkontakteAccessToken
     * @return $this
     */
    public function setVkontakteAccessToken(string $vkontakteAccessToken)
    {
        $this->vkontakteAccessToken = $vkontakteAccessToken;
        return $this;
    }

    /**
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * @param string $facebookId
     * @return $this
     */
    public function setFacebookId(string $facebookId)
    {
        $this->facebookId = $facebookId;
        return $this;
    }

    /**
     * @return string
     */
    public function getFacebookAccessToken()
    {
        return $this->facebookAccessToken;
    }

    /**
     * @param string $facebookAccessToken
     * @return $this
     */
    public function setFacebookAccessToken(string $facebookAccessToken)
    {
        $this->facebookAccessToken = $facebookAccessToken;
        return $this;
    }

    /**
     * @return string
     */
    public function getTwitterId()
    {
        return $this->twitterId;
    }

    /**
     * @param string $twitterId
     * @return $this
     */
    public function setTwitterId(string $twitterId)
    {
        $this->twitterId = $twitterId;
        return $this;
    }

    /**
     * @return string
     */
    public function getTwitterAccessToken()
    {
        return $this->twitterAccessToken;
    }

    /**
     * @param string $twitterAccessToken
     * @return $this
     */
    public function setTwitterAccessToken(string $twitterAccessToken)
    {
        $this->twitterAccessToken = $twitterAccessToken;
        return $this;
    }

}
