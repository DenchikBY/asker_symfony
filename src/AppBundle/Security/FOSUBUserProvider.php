<?php

namespace AppBundle\Security;

use AppBundle\Entity\UserSocials;
use AppBundle\Repository\UserSocialsRepository;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserManagerInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseClass;
use Symfony\Component\Security\Core\User\UserInterface;

class FOSUBUserProvider extends BaseClass
{

    /** @var EntityManager */
    protected $em;

    /** @var UserSocialsRepository */
    protected $userSocialsRepository;

    public function __construct(UserManagerInterface $userManager,
                                UserSocialsRepository $userSocialsRepository,
                                EntityManager $entityManager, array $properties)
    {
        parent::__construct($userManager, $properties);
        $this->em = $entityManager;
        $this->userSocialsRepository = $userSocialsRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function connect(UserInterface $user, UserResponseInterface $response)
    {
        $property = $this->getProperty($response);
        $service = $response->getResourceOwner()->getName();
        $username = $response->getUsername();
        $setter = 'set' . ucfirst($service);
        $setterId = $setter . 'Id';
        $setterToken = $setter . 'AccessToken';
        if (null !== $previousUserSocials = $this->userSocialsRepository->findOneBy([$property => $username])) {
            $previousUserSocials->$setterId(null);
            $previousUserSocials->$setterToken(null);
            $this->em->persist($previousUserSocials);
        }
        $userSocials = $user->getUserSocials();
        $userSocials->$setterId($username);
        $userSocials->$setterToken($response->getAccessToken());
        $this->em->persist($userSocials);
        $this->em->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $username = $response->getUsername();
        $userSocials = $this->userSocialsRepository->findOneBy([$this->getProperty($response) => $username]);
        if (null === $userSocials) {
            $service = $response->getResourceOwner()->getName();
            $setter = 'set' . ucfirst($service);
            $setterId = $setter . 'Id';
            $setterToken = $setter . 'AccessToken';
            $user = $this->userManager->createUser()
                ->setUsername($response->getResourceOwner()->getName() . '_' . $username)
                ->setEmail($response->getEmail())
                ->setPassword($username)
                ->setEnabled(true);
            $this->userManager->updateUser($user);
            $userSocials = $user->getUserSocials() ?? new UserSocials();
            $userSocials->setUser($user);
            $userSocials->$setterId($username);
            $userSocials->$setterToken($response->getAccessToken());
            $this->em->persist($userSocials);
            $this->em->flush();
            return $user;
        }
        $user = $userSocials->getUser();
        $serviceName = $response->getResourceOwner()->getName();
        $setter = 'set' . ucfirst($serviceName) . 'AccessToken';
        $user->getUserSocials()->$setter($response->getAccessToken());
        return $user;
    }

}
