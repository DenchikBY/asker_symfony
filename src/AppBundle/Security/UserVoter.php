<?php

namespace AppBundle\Security;

use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class UserVoter extends Voter
{

    const ASK = 'ask';
    const FOLLOW = 'follow';
    const LIKE = 'like';
    const EQUAL = 'equal';

    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [self::ASK, self::FOLLOW, self::EQUAL])) {
            return false;
        }
        if (!$subject instanceof User) {
            return false;
        }
        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $currentUser = $token->getUser();
        if (!($currentUser instanceof User)) $currentUser = null;

        /** @var User $user */
        $user = $subject;

        switch ($attribute) {
            case self::ASK:
                return $this->canAsk($user, $token, $currentUser);
            case self::FOLLOW:
                return $this->canFollow($user, $token, $currentUser);
            case self::LIKE:
                return $this->canLike($user, $token, $currentUser);
            case self::EQUAL:
                return $this->isEqual($user, $token, $currentUser);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canAsk(User $user, TokenInterface $token, $currentUser): bool
    {
        if ($currentUser === null) return true;
        return $this->decisionManager->decide($token, [User::ROLE_USER]) && $user->getId() !== $currentUser->getId();
    }

    private function canFollow(User $user, TokenInterface $token, $currentUser): bool
    {
        return $this->decisionManager->decide($token, [User::ROLE_USER]) && $user->getId() !== $currentUser->getId();
    }

    private function canLike(User $user, TokenInterface $token, $currentUser): bool
    {
        return $this->decisionManager->decide($token, [User::ROLE_USER]) && $user->getId() !== $currentUser->getId();
    }

    private function isEqual(User $user, TokenInterface $token, $currentUser): bool
    {
        return $this->decisionManager->decide($token, [User::ROLE_USER]) && $user->getId() === $currentUser->getId();
    }

}
