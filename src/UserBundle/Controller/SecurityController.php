<?php

namespace UserBundle\Controller;

use AppBundle\Entity\User;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SecurityController extends BaseController
{

    /** @var Request */
    private $request;

    public function loginAction(Request $request)
    {
        if ($this->isGranted(User::ROLE_USER))
            return $this->redirectToRoute('site_homepage');
        $this->request = $request;
        return parent::loginAction($request);
    }

    public function captchaAction()
    {
        return new Response('', 204);
    }

}
