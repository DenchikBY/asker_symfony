<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations as JMS;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\View\View;

/**
 * @RouteResource("User", pluralize=false)
 * @JMS\View(serializerGroups={"default"})
 */
class UserController extends FOSRestController implements ClassResourceInterface
{

    /**
     * @return View
     */
    public function getSelfAction(): View
    {
        return $this->view(['data' => $this->getUser()], 200);
    }

    /**
     * @return View
     */
    public function getAllAction(): View
    {
        $users = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();
        return $this->view($users);
    }

    /**
     * @JMS\View(serializerGroups={"default", "followedByViewer"})
     * @param int $id
     * @return View
     */
    public function getAction(int $id): View
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:User');
        $user = $repository->find($id);
        return $this->view(['data' => $user]);
    }

}
