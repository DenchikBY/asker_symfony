<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{

    /**
     * @Route("/", name="admin:homepage")
     * @return Response
     */
    public function indexAction(): Response
    {
        return $this->render('@Admin/Index/index.html.twig');
    }

}
