<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\ProfileSettingsType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/account/settings")
 * @Security("has_role('ROLE_USER')")
 */
class SettingsController extends AbstractController
{

    /**
     * @Route("/profile", name="site_account_settings_profile", methods={"GET"})
     * @Template()
     * @return Response|array
     */
    public function profileSettingsAction(): array
    {
        $form = $this->createForm(ProfileSettingsType::class);
        $form->setData($this->getUser());
        return ['form' => $form->createView()];
    }

    /**
     * @Route("/profile", name="site_account_settings_profile_save", methods={"PUT"})
     * @param Request $request
     * @return Response
     */
    public function profileSettingsSaveAction(Request $request): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(ProfileSettingsType::class);
        $form->setData($user);
        $form->handleRequest($request);
        if (!$form->isValid()) {
            return $this->jsonFormErrors($form);
        }
        $this->getUserRepository()->update($user);
        return $this->jsonUrl($this->generateUrl('site_profile_show', ['username' => $this->getUser()->getUsername()]));
    }

    /**
     * @Route("/notifications", name="site_account_settings_notifications", methods={"GET"})
     * @Template()
     * @return Response|array
     */
    public function notificationsSettingsAction(): array
    {
        return [];
    }

    /**
     * @Route("/notifications", name="site_account_settings_notifications_save", methods={"PUT"})
     * @param Request $request
     * @return Response
     */
    public function notificationsSettingsSaveAction(Request $request): Response
    {
        $newQuestionEmail = $request->get('new_question_email', 'true') === 'true' ? true : false;
        /** @var User $user */
        $user = $this->getUser();
        $user->setNewQuestionEmail($newQuestionEmail);
        $this->getUserRepository()->update($user);
        return $this->redirectBack();
    }

}
