<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Question;
use AppBundle\Entity\User;
use AppBundle\Form\AskType;
use AppBundle\Security\UserVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/{username}")
 */
class ProfileController extends AbstractController
{

    /**
     * @Route("/", methods={"GET"}, name="site_profile_show")
     * @Template()
     * @param string $username
     * @return Response|array
     */
    public function profileAction(string $username)
    {
        $user = $this->getUserRepository()->findByUsername($username, $this->getUser());
        if ($user == null) throw $this->createNotFoundException();
        $questions = $this->getQuestionRepository()->getAnswered($user);
        $askForm = null;
        if ($this->isGranted(UserVoter::ASK, $user)) {
            $askForm = $this->createForm(AskType::class, null, [
                'action' => $this->generateUrl('site_profile_ask', ['username' => $user->getUsername()])
            ])->createView();
        }
        return [
            'user' => $user,
            'askForm' => $askForm,
            'questions' => $questions
        ];
    }

    /**
     * @Route("/best", methods={"GET"}, name="site_profile_best")
     * @Template(template="@App/Profile/profile.html.twig")
     * @param string $username
     * @return Response|array
     */
    public function bestAction(string $username)
    {
        return $this->profileAction($username);
    }

    /**
     * @Route("/ask", methods={"GET"}, name="site_profile_ask_form")
     * @Template()
     * @param User $user
     * @return array|Response
     */
    public function askFormAction(User $user): array
    {
        $this->denyAccessUnlessGranted(UserVoter::ASK, $user);
        return ['user' => $user];
    }

    /**
     * @Route("/ask", methods={"POST"}, name="site_profile_ask")
     * @param User $user
     * @param Request $request
     * @return Response
     */
    public function askAction(User $user, Request $request): Response
    {
        $this->denyAccessUnlessGranted(UserVoter::ASK, $user);
        $questionText = $request->get('question_text');
        $isAnonymous = (bool)$request->get('anonymous');
        $length = strlen($questionText);
        if ($length === 0 || $length > 300) {
            return $this->jsonErrors('Вы забыли заполнить некоторые важные поля', ['question_text']);
        }
        $question = (new Question())
            ->setSender($isAnonymous ? null : $this->getUser())
            ->setReceiver($user)
            ->setText($questionText);
        $this->getQuestionRepository()->save($question);
        return $this->jsonUrl($this->generateUrl('site_profile_show', ['username' => $user->getUsername()]));
    }

    /**
     * @Route("/follow", methods={"GET"}, name="site_profile_follow")
     * @Security("has_role('ROLE_USER')")
     * @param User $user
     * @return Response
     */
    public function followAction(User $user): Response
    {
        $this->denyAccessUnlessGranted(UserVoter::FOLLOW, $user);
        $this->getFollowRepository()->follow($this->getUser(), $user);
        return $this->redirectToRoute('site_profile_show', ['username' => $user->getUsername()]);
    }

    /**
     * @Route("/unfollow", methods={"GET"}, name="site_profile_unfollow")
     * @Security("has_role('ROLE_USER')")
     * @param User $user
     * @return Response
     */
    public function unFollowAction(User $user): Response
    {
        $this->denyAccessUnlessGranted(UserVoter::FOLLOW, $user);
        $this->getFollowRepository()->unFollow($this->getUser(), $user);
        return $this->redirectToRoute('site_profile_show', ['username' => $user->getUsername()]);
    }

}
