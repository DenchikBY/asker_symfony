<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Question;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;

class QuestionListener
{

    /** @var \Swift_Mailer */
    private $mailer;

    /** @var \Twig_Environment */
    private $twig;

    /** @var string */
    private $mailerAddress;

    /** @var string */
    private $mailerSenderName;

    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig, string $mailerAddress, $mailerSenderName)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->mailerAddress = $mailerAddress;
        $this->mailerSenderName = $mailerSenderName;
    }

    /**
     * @ORM\PostPersist
     * @param Question $question
     * @param LifecycleEventArgs $event
     */
    public function newQuestionNotification(Question $question, LifecycleEventArgs $event)
    {
        if ($question->getReceiver()->isNewQuestionEmail()) {
            $message = \Swift_Message::newInstance()
                ->setContentType('text/html')
                ->setSubject('New question')
                //->setFrom("\"{$this->mailerSenderName}\" <{$this->mailerAddress}>")
                ->setFrom($this->mailerAddress)
                ->setTo($question->getReceiver()->getEmail())
                ->setBody(
                    $this->twig->render(
                        '@App/Email/new_question.html.twig', [
                            'question' => $question
                        ]
                    )
                );
            $this->mailer->send($message);
        }
    }

}
