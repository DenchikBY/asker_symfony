<?php

namespace AppBundle\Controller;

use AppBundle\EventListener\LocaleListener;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\{
    Cookie, Request, Response
};

class IndexController extends AbstractController
{

    /**
     * @Route("/", name="site_homepage")
     * @Template()
     * @return Response|array
     */
    public function indexAction(Request $request)
    {
        if ($this->getUser() != null) {
            return $this->redirectToRoute('site_account_wall');
        }
        $userRepository = $this->getUserRepository();
        return [
            'users' => $userRepository->getRandomUsers(24),
        ];
    }

    /**
     * @Route("/home/languages", name="site_home_languages")
     * @Template(template="@App/Index/locales_modal.html.twig")
     * @return Response|array
     */
    public function localesAction()
    {
        return ['locales' => $this->getLocaleRepository()->getAll()];
    }

    /**
     * @Route("/home/languages/{locale}", name="site_change_language")
     * @param string $locale
     * @return Response
     */
    public function changeLocaleAction(string $locale)
    {
        $response = $this->redirectToRoute('site_homepage');
        $localeEntity = $this->getLocaleRepository()->getByCode($locale);
        if ($localeEntity != null)
            $response->headers->setCookie(new Cookie(LocaleListener::KEY, $localeEntity->getCode(), time() + 31557600));
        return $response;
    }

    /**
     * @Route("/home/menu", name="site_home_menu")
     * @Template()
     * @return Response|array
     */
    public function homeMenuAction(): array
    {
        return [];
    }

}
