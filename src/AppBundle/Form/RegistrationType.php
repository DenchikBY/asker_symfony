<?php

namespace AppBundle\Form;

use AppBundle\Entity\Locale;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\{
    BirthdayType, ChoiceType, EmailType, PasswordType, TextType
};
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{

    /** @var EntityManager */
    private $entityManager;

    /** @var RequestStack */
    private $requestStack;

    public function __construct(EntityManager $entityManager, RequestStack $requestStack)
    {
        $this->entityManager = $entityManager;
        $this->requestStack = $requestStack;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $localeRepository = $this->entityManager->getRepository('AppBundle:Locale');
        $builder
            ->add('username', TextType::class, ['label' => false, 'required' => false, 'attr' => [
                'placeholder' => 'form.registration.username',
                'maxlength' => 40,
                'autocomplete' => 'username',
                'autofocus' => 'autofocus',
                'data-input' => 'SignupCheck',
                'data-url' => '/signup/check',
                'size' => 40
            ]])
            ->add('plainPassword', PasswordType::class, ['label' => false, 'required' => false, 'attr' => [
                'placeholder' => 'form.registration.password',
                'maxlength' => 20,
                'autocomplete' => 'new-password',
                'size' => 20
            ]])
            ->add('fullName', TextType::class, ['label' => false, 'required' => false, 'attr' => [
                'placeholder' => 'form.registration.name',
                'maxlength' => 30,
                'autocomplete' => 'name',
                'size' => 30
            ]])
            ->add('email', EmailType::class, ['label' => false, 'required' => false, 'attr' => [
                'placeholder' => 'form.registration.email',
                'maxlength' => 50,
                'size' => 50
            ]])
            ->add('bornAt', BirthdayType::class, [
                'label' => 'form.registration.birthday',
                'required' => false,
                'format' => 'dd-MMMM-yyyy',
                'placeholder' => [
                    'year' => 'form.registration.year',
                    'month' => 'form.registration.month',
                    'day' => 'form.registration.day',
                ],
                'years' => range(date('Y'), date('Y')-100)
            ])
            ->add('gender', ChoiceType::class, [
                'label' => 'form.registration.my_gender',
                'required' => false,
                'attr'=> ['class' => 'simpleFormItem'],
                'choices' => [
                    'empty' => '',
                    'form.registration.female' => 1,
                    'form.registration.male' => 2
                ]
            ])
            ->add('localeCode', EntityType::class, [
                'label' => 'form.registration.locale',
                'required' => false,
                'attr'=> ['class' => 'simpleFormItem'],
                'class' => Locale::class,
                'choices' => $localeRepository->getAll(),
                'choice_label' => 'name',
                'choice_value' => 'code',
                'data' => (new Locale())->setCode($this->requestStack->getCurrentRequest()->getLocale())
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'csrf_token_id' => 'registration',
            'allow_extra_fields' => true,
            'attr' => [
                'id' => 'signupForm',
                'autocomplete' => 'off',
                'class' => 'new_user',
                'data-action' => 'SubmitWithTimezone',
                'accept-charset' => 'UTF-8',
            ]
        ]);
    }

    public function getBlockPrefix()
    {
        return 'registration';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }

}
