<?php

namespace AppBundle\Controller;

use AppBundle\Repository\{
    EventRepository, FollowRepository, LocaleRepository, QuestionLikeRepository, QuestionRepository, UserRepository, UserSocialsRepository
};
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

abstract class AbstractController extends Controller
{

    /**
     * @return EventRepository
     */
    protected function getEventRepository(): EventRepository
    {
        return $this->getDoctrine()->getRepository('AppBundle:Event');
    }

    /**
     * @return FollowRepository
     */
    protected function getFollowRepository(): FollowRepository
    {
        return $this->getDoctrine()->getRepository('AppBundle:Follow');
    }

    /**
     * @return LocaleRepository
     */
    protected function getLocaleRepository(): LocaleRepository
    {
        return $this->getDoctrine()->getRepository('AppBundle:Locale');
    }

    /**
     * @return QuestionLikeRepository
     */
    protected function getQuestionLikeRepository(): QuestionLikeRepository
    {
        return $this->getDoctrine()->getRepository('AppBundle:QuestionLike');
    }

    /**
     * @return QuestionRepository
     */
    protected function getQuestionRepository(): QuestionRepository
    {
        return $this->getDoctrine()->getRepository('AppBundle:Question');
    }

    /**
     * @return UserRepository
     */
    protected function getUserRepository(): UserRepository
    {
        return $this->getDoctrine()->getRepository('AppBundle:User');
    }

    /**
     * @return UserSocialsRepository
     */
    protected function getUserSocialsRepository(): UserSocialsRepository
    {
        return $this->getDoctrine()->getRepository('AppBundle:UserSocials');
    }

    /**
     * @param mixed $data
     * @param int $status
     * @param array $headers
     * @param array $context
     * @return JsonResponse
     */
    protected function json($data, $status = 200, $headers = [], $context = []): JsonResponse
    {
        return (new JsonResponse())->setJson($this->get('serializer')->serialize($data, 'json'));
    }

    /**
     * @return string
     */
    protected function getLastUrl(): string
    {
        $url = $this->get('request_stack')->getCurrentRequest()->headers->get('referer');
        if (strlen($url) === 0)
            return $this->generateUrl('site_homepage');
        $parsedUrl = parse_url($url);
        return $parsedUrl['path'];
    }

    /**
     * @return RedirectResponse
     */
    protected function redirectBack(): RedirectResponse
    {
        return $this->redirect($this->getLastUrl());
    }

    /**
     * @param string $error
     * @param array $fields
     * @return JsonResponse
     */
    protected function jsonErrors(string $error, array $fields): JsonResponse
    {
        return $this->json(['error' => $error, 'fields' => $fields]);
    }

    /**
     * @param FormInterface $form
     * @return JsonResponse
     */
    protected function jsonFormErrors(FormInterface $form): JsonResponse
    {
        $fields = [];
        $errors = $form->getErrors(true);
        $message = $errors->current()->getMessage();
        foreach ($form->all() as $field) {
            if (!$field->isValid()) {
                $subFields = $field->all();
                if (empty($subFields)) {
                    $fields[] = "{$form->getName()}[{$field->getName()}]";
                } else {
                    foreach ($subFields as $subField) {
                        $fields[] = "{$form->getName()}[{$field->getName()}][{$subField->getName()}]";
                    }
                }
            }
        }
        return $this->jsonErrors($message, $fields);
    }

    /**
     * @param string $url
     * @return JsonResponse
     */
    protected function jsonUrl(string $url): JsonResponse
    {
        return $this->json(['url' => $url]);
    }

    /**
     * @return JsonResponse
     */
    protected function jsonLastUrl(): JsonResponse
    {
        return $this->jsonUrl($this->getLastUrl());
    }

}
