<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Question;
use AppBundle\Entity\User;

class QuestionRepository extends AbstractRepository
{

    /**
     * @param User $user
     * @return int
     */
    public function getNotAnsweredCount(User $user): int
    {
        return $this->createQueryBuilder('q')
            ->select('count(q.id)')
            ->where('q.receiver = :receiver')
            ->andWhere('q.isAnswered = false')
            ->orderBy('q.createdAt', 'desc')
            ->setParameter('receiver', $user)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param User $user
     * @return Question[]|null
     */
    public function getNotAnswered(User $user)
    {
        return $this->createQueryBuilder('q')
            ->where('q.receiver = :receiver')
            ->andWhere('q.isAnswered = false')
            ->orderBy('q.createdAt', 'desc')
            ->setParameter('receiver', $user)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param User $user
     * @return Question[]|null
     */
    public function getAnswered(User $user)
    {
        return $this->createQueryBuilder('q')
            ->where('q.receiver = :receiver')
            ->andWhere('q.isAnswered = true')
            ->orderBy('q.createdAt', 'desc')
            ->setParameter('receiver', $user)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param User $user
     */
    public function deleteAllNewQuestions(User $user)
    {
        $this->getEntityManager()
            ->createQuery('delete from AppBundle:Question q where q.receiver = :user and q.isAnswered = false')
            ->setParameter('user', $user)
            ->execute();
    }

}
