<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Event;
use AppBundle\Entity\Question;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/account/private-questions", requirements={"id": "\d+"})
 * @Security("has_role('ROLE_USER')")
 */
class QuestionController extends AbstractController
{

    /**
     * @Route("", methods={"DELETE"}, name="site_question_delete_all")
     * @return Response
     */
    public function deleteAllAction(): Response
    {
        $this->getQuestionRepository()->deleteAllNewQuestions($this->getUser());
        return new Response('', 204);
    }

    /**
     * @Route("/{id}", methods={"GET"}, name="site_question_show")
     * @Template()
     * @param Question $question
     * @return Response|array
     */
    public function showAction(Question $question): array
    {
        return ['question' => $question];
    }

    /**
     * @Route("/{id}", methods={"POST"}, name="site_question_answer")
     * @param Question $question
     * @param Request $request
     * @return Response
     */
    public function answerAction(Question $question, Request $request): Response
    {
        $fieldName = 'answer_text';
        $answerText = $request->request->get($fieldName);
        if (strlen($answerText) === 0) {
            return $this->jsonErrors('Вы забыли заполнить некоторые важные поля', [$fieldName]);
        }
        $question
            ->setIsAnswered(true)
            ->setAnswerText($answerText)
            ->setAnsweredAt(new \DateTime());
        $this->getQuestionRepository()->update($question);
        $event = (new Event())
            ->setQuestion($question)
            ->setType(Event::TYPE_ANSWER)
            ->setQuestionReceiverUser($question->getReceiver())
            ->setEventUser($this->getUser());
        $this->getEventRepository()->save($event);
        return $this->jsonUrl($this->generateUrl('site_account_inbox'));
    }

    /**
     * @Route("/{id}", methods={"DELETE"}, name="site_question_delete")
     * @param Question $question
     * @return Response
     */
    public function deleteAction(Question $question): Response
    {
        $this->getQuestionRepository()->delete($question);
        return new Response('', 204);
    }

    /**
     * @Route("/{id}/like", methods={"GET"}, name="site_question_like")
     * @param Question $question
     * @return Response
     */
    public function likeAction(Question $question): Response
    {
        $this->getQuestionLikeRepository()->like($this->getUser(), $question);
        return $this->redirectToRoute('site_profile_show', ['username' => $question->getReceiver()->getUsername()]);
    }

    /**
     * @Route("/{id}/dislike", methods={"GET"}, name="site_question_dislike")
     * @param Question $question
     * @return Response
     */
    public function dislikeAction(Question $question): Response
    {
        $this->getQuestionLikeRepository()->dislike($this->getUser(), $question);
        return $this->redirectToRoute('site_profile_show', ['username' => $question->getReceiver()->getUsername()]);
    }

    /**
     * @Route("/{id}/flag/report", name="site_question_flag_report")
     * @param Question $question
     * @return Response
     */
    public function flagReportAction(Question $question): Response
    {
        return $this->json([]);
    }

    /**
     * @Route("/{id}/flag/block", name="site_question_flag_block")
     * @param Question $question
     * @return Response
     */
    public function flagBlockAction(Question $question): Response
    {
        return $this->json([]);
    }

}
