<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Locale;
use Doctrine\ORM\EntityRepository;

class LocaleRepository extends EntityRepository
{

    /**
     * @return Locale[]|null
     */
    public function getAll()
    {
        return $this->createQueryBuilder('l')
            ->getQuery()
            ->useResultCache(true, 3600, 'all_languages')
            ->getResult();
    }

}
