<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Locale;
use Doctrine\ORM\EntityRepository;

class LocaleRepository extends EntityRepository
{

    /**
     * @return Locale[]|null
     */
    public function getAll()
    {
        return $this->createQueryBuilder('l')
            ->getQuery()
            ->useResultCache(true, 3600, 'all_languages')
            ->getResult();
    }

    /**
     * @param string $code
     * @return Locale|null
     */
    public function getByCode(string $code)
    {
        return $this->createQueryBuilder('l')
            ->where('l.code = :code')
            ->setParameter('code', $code)
            ->getQuery()
            ->getSingleResult();
    }

}
