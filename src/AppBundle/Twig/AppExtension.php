<?php

namespace AppBundle\Twig;

use AppBundle\Entity\Locale;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class AppExtension extends \Twig_Extension implements \Twig_Extension_InitRuntimeInterface, \Twig_Extension_GlobalsInterface
{

    /** @var UrlGeneratorInterface */
    private $generator;

    /** @var bool */
    private $isPrompt;

    /** @var RequestStack */
    private $requestStack;

    /** @var EntityManager */
    private $em;

    public function __construct(RequestStack $requestStack, UrlGeneratorInterface $generator,
                                TokenStorage $tokenStorage, EntityManager $em)
    {
        $this->requestStack = $requestStack;
        $this->generator = $generator;
        $this->em = $em;
        $token = $tokenStorage->getToken();
        $this->isPrompt = $token === null || !$token->isAuthenticated();
    }

    /**
     * @return \Twig_SimpleFunction[]
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('prompt_path', [$this, 'getPromptPath']),
        ];
    }

    public function getPromptPath($name, $parameters = [], $relative = false)
    {
        if ($this->isPrompt) return $this->generator->generate('user_prompt');
        return $this->generator->generate($name, $parameters, $relative ? UrlGeneratorInterface::RELATIVE_PATH : UrlGeneratorInterface::ABSOLUTE_PATH);
    }

    public function getGlobals()
    {
        return [
            'currentLocale' => $this->getCurrentLocale(),
        ];
    }

    /**
     * @return Locale
     */
    private function getCurrentLocale()
    {
        $locale = $this->requestStack->getCurrentRequest()->getLocale();
        return $this->em
            ->getRepository('AppBundle:Locale')
            ->getByCode($locale);
    }

}
