<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Table(name="follows", indexes={
 *     @Index(columns={"follower_id"}),
 *     @Index(columns={"followed_id"}),
 *     @Index(columns={"follower_id", "followed_id"})
 * })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FollowRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Follow
{

    use BaseEntity;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="followed")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @var User
     */
    protected $follower;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="followers")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @var User
     */
    protected $followed;

    /**
     * @return User
     */
    public function getFollower()
    {
        return $this->follower;
    }

    /**
     * @param User $follower
     * @return $this
     */
    public function setFollower(User $follower)
    {
        $this->follower = $follower;
        return $this;
    }

    /**
     * @return User
     */
    public function getFollowed()
    {
        return $this->followed;
    }

    /**
     * @param User $followed
     * @return $this
     */
    public function setFollowed(User $followed)
    {
        $this->followed = $followed;
        return $this;
    }

}
