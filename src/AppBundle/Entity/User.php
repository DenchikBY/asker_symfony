<?php

namespace AppBundle\Entity;

use AppBundle\Form\ProfileSettings;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @UniqueEntity(fields={"email", "username"})
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser implements UserInterface
{

    use BaseEntity;

    const ROLE_USER = 'ROLE_USER';

    const GENDER_FEMALE = 1;
    const GENDER_MALE = 2;

    const SIDE_MENU_FOLLOWS_COUNT = 6;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * @Assert\NotNull()
     * @Assert\Length(min="4")
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    protected $fullName;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    protected $location;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    protected $biography;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    protected $website;

    /**
     * @Assert\NotBlank()
     * @Assert\Date()
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    protected $bornAt;

    /**
     * @Assert\Choice(choices={User::GENDER_FEMALE, User::GENDER_MALE})
     * @ORM\Column(type="smallint", nullable=true)
     * @var int
     */
    protected $gender;

    /**
     * ORM\Column(type="string", nullable=true)
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Locale")
     * @ORM\JoinColumn(name="locale_code", referencedColumnName="code", onDelete="SET NULL")
     * @var string
     */
    protected $locale;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\UserSocials", mappedBy="user", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     * @var UserSocials
     */
    protected $userSocials;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    protected $answersCount = 0;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    protected $likesCount = 0;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    protected $followersCount = 0;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    protected $followedCount = 0;

    /**
     * @Assert\Range(min="0",  max="17")
     * @ORM\Column(type="smallint", name="theme_id")
     * @var int
     */
    protected $theme = 0;

    /**
     * @Assert\Range(min="0", max="49")
     * @ORM\Column(type="smallint", name="emoodji_id")
     * @var int
     */
    protected $emoodji = 0;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    protected $allowAnonymousQuestions = true;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    protected $allowAnswerSharing = true;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    protected $allowSubscribing = true;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    protected $allowDiscovery = true;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    protected $showOnlineStatus = true;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    protected $newQuestionEmail = true;

    /**
     * @var boolean
     */
    protected $followedByViewer = false;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Follow", mappedBy="followed", cascade={"remove"})
     */
    protected $followers;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Follow", mappedBy="follower", cascade={"remove"})
     */
    protected $followed;

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->fullName ?? $this->username;
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param string $location
     * @return $this
     */
    public function setLocation(string $location)
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return string
     */
    public function getBiography()
    {
        return $this->biography;
    }

    /**
     * @param string $biography
     * @return $this
     */
    public function setBiography(string $biography)
    {
        $this->biography = $biography;
        return $this;
    }

    /**
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param mixed $website
     * @return $this
     */
    public function setWebsite(string $website)
    {
        $this->website = $website;
        return $this;
    }

    /**
     * @param string $fullName
     * @return $this
     */
    public function setFullName(string $fullName)
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBornAt()
    {
        return $this->bornAt;
    }

    /**
     * @param \DateTime $bornAt
     * @return $this
     */
    public function setBornAt($bornAt)
    {
        $this->bornAt = $bornAt;
        return $this;
    }

    /**
     * @return int
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param int $gender
     * @return $this
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     * @return $this
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return UserSocials
     */
    public function getUserSocials()
    {
        return $this->userSocials;
    }

    /**
     * @param UserSocials $userSocials
     * @return $this
     */
    public function setUserSocials(UserSocials $userSocials)
    {
        $this->userSocials = $userSocials;
        return $this;
    }

    /**
     * @return int
     */
    public function getAnswersCount()
    {
        return $this->answersCount;
    }

    /**
     * @param int $answersCount
     * @return $this
     */
    public function setAnswersCount(int $answersCount)
    {
        $this->answersCount = $answersCount;
        return $this;
    }

    /**
     * @return int
     */
    public function getLikesCount()
    {
        return $this->likesCount;
    }

    /**
     * @param int $likesCount
     * @return $this
     */
    public function setLikesCount(int $likesCount)
    {
        $this->likesCount = $likesCount;
        return $this;
    }

    /**
     * @return int
     */
    public function getFollowersCount()
    {
        return $this->followersCount;
    }

    /**
     * @param int $followersCount
     * @return $this
     */
    public function setFollowersCount(int $followersCount)
    {
        $this->followersCount = $followersCount;
        return $this;
    }

    /**
     * @return int
     */
    public function getFollowedCount()
    {
        return $this->followedCount;
    }

    /**
     * @param int $followedCount
     * @return $this
     */
    public function setFollowedCount(int $followedCount)
    {
        $this->followedCount = $followedCount;
        return $this;
    }

    /**
     * @return int
     */
    public function getTheme(): int
    {
        return $this->theme;
    }

    /**
     * @param int $theme
     * @return $this
     */
    public function setTheme(int $theme)
    {
        $this->theme = $theme;
        return $this;
    }

    /**
     * @return int
     */
    public function getEmoodji(): int
    {
        return $this->emoodji;
    }

    /**
     * @param int $emoodji
     * @return $this
     */
    public function setEmoodji(int $emoodji)
    {
        $this->emoodji = $emoodji;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAllowAnonymousQuestions(): bool
    {
        return $this->allowAnonymousQuestions;
    }

    /**
     * @param bool $allowAnonymousQuestions
     * @return $this
     */
    public function setAllowAnonymousQuestions(bool $allowAnonymousQuestions)
    {
        $this->allowAnonymousQuestions = $allowAnonymousQuestions;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAllowAnswerSharing(): bool
    {
        return $this->allowAnswerSharing;
    }

    /**
     * @param bool $allowAnswerSharing
     * @return $this
     */
    public function setAllowAnswerSharing(bool $allowAnswerSharing)
    {
        $this->allowAnswerSharing = $allowAnswerSharing;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAllowSubscribing(): bool
    {
        return $this->allowSubscribing;
    }

    /**
     * @param bool $allowSubscribing
     * @return $this
     */
    public function setAllowSubscribing(bool $allowSubscribing)
    {
        $this->allowSubscribing = $allowSubscribing;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAllowDiscovery(): bool
    {
        return $this->allowDiscovery;
    }

    /**
     * @param bool $allowDiscovery
     * @return $this
     */
    public function setAllowDiscovery(bool $allowDiscovery)
    {
        $this->allowDiscovery = $allowDiscovery;
        return $this;
    }

    /**
     * @return bool
     */
    public function isShowOnlineStatus(): bool
    {
        return $this->showOnlineStatus;
    }

    /**
     * @param bool $showOnlineStatus
     * @return $this
     */
    public function setShowOnlineStatus(bool $showOnlineStatus)
    {
        $this->showOnlineStatus = $showOnlineStatus;
        return $this;
    }

    /**
     * @return bool
     */
    public function isNewQuestionEmail(): bool
    {
        return $this->newQuestionEmail;
    }

    /**
     * @param bool $newQuestionEmail
     * @return $this
     */
    public function setNewQuestionEmail(bool $newQuestionEmail)
    {
        $this->newQuestionEmail = $newQuestionEmail;
        return $this;
    }

    /**
     * @return bool
     */
    public function isFollowedByViewer(): bool
    {
        return $this->followedByViewer;
    }

    /**
     * @param bool $followedByViewer
     * @return $this
     */
    public function setFollowedByViewer(bool $followedByViewer)
    {
        $this->followedByViewer = $followedByViewer;
        return $this;
    }

}
