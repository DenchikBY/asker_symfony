<?php

namespace UserBundle\Controller;

use AppBundle\Entity\User;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use FOS\UserBundle\Event\{
    FilterUserResponseEvent, FormEvent, GetResponseUserEvent
};
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class RegistrationController extends BaseController
{

    public function registerAction(Request $request)
    {
        if ($this->isGranted(User::ROLE_USER))
            return $this->redirectToRoute('site_homepage');

        return parent::registerAction($request);

        /** @var $formFactory FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');
        /** @var Session $session */
        $session = $this->get('session');

        if ($request->isMethod('POST')) {

            $user = $userManager->createUser();
            $user->setEnabled(true);

            $event = new GetResponseUserEvent($user, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);
            if (null !== $event->getResponse()) {
                return $event->getResponse();
            }
            $form = $formFactory->createForm();
            $form->setData($user);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);
                $userManager->updateUser($user);
                if (null === $response = $event->getResponse()) {
                    $response = $this->redirectToRoute('site_homepage');
                }
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
                return $response;
            }
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_FAILURE, $event);

            if (null !== $response = $event->getResponse()) {
                return $response;
            }

            $session->getFlashBag()->set($form->getName(), $request->request->all());
            return $this->redirectToRoute('fos_user_registration_register');
        } else {
            $form = $formFactory->createForm();
            if ($session->getFlashBag()->has($form->getName())) {
                //$form->submit($session->getFlashBag()->get($form->getName())[$form->getName()]);
            }
            return $this->render('@FOSUser/Registration/register.html.twig', [
                'form' => $form->createView(),
                'locales' => $this->getParameter('locales'),
            ]);
        }
    }

    public function checkEmailAction()
    {
        if ($this->isGranted(User::ROLE_USER))
            return $this->redirectToRoute('site_homepage');
        return parent::checkEmailAction();
    }

    public function confirmAction(Request $request, $token)
    {
        if ($this->isGranted(User::ROLE_USER))
            return $this->redirectToRoute('site_homepage');
        return parent::confirmAction($request, $token);
    }

    public function confirmedAction()
    {
        if ($this->isGranted(User::ROLE_USER))
            return $this->redirectToRoute('site_homepage');
        return parent::confirmedAction();
    }

    public function promptAction(Request $request)
    {
        return $this->render('@FOSUser/Registration/register.html.twig', [
            'locales' => $this->getParameter('locales'),
            'prompt' => true,
        ]);
    }

    public function captchaAction()
    {
        return new Response('', 204);
    }

}
