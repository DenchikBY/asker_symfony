<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Question;
use AppBundle\Security\UserVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/account")
 * @Template()
 * @Security("has_role('ROLE_USER')")
 */
class AccountController extends AbstractController
{

    /**
     * @Route("/wall", name="site_account_wall")
     * @return Response|array
     */
    public function wallAction()
    {
//        $this->get('old_sound_rabbit_mq.test_producer')
//            ->publish(json_encode(['name' => 'Denchik1234']));
        $eventRepository = $this->getEventRepository();
        $followRepository = $this->getFollowRepository();
        return [
            'events' => $eventRepository->getFeed($this->getUser()),
            'follows' => $followRepository->getUserLastFollows($this->getUser())
        ];
    }

    /**
     * @Route("/inbox", name="site_account_inbox")
     * @return Response|array
     */
    public function inboxAction()
    {
        $questionRepository = $this->getQuestionRepository();
        $questions = $questionRepository->getNotAnswered($this->getUser());
        return ['questions' => $questions];
    }

    /**
     * @Route("/friends", name="site_account_friends")
     * @return Response|array
     */
    public function friendsAction()
    {
        return [];
    }

    /**
     * @Route("/notifications", name="site_account_notifications")
     * @return Response|array
     */
    public function notificationsAction()
    {
        return [];
    }

    /**
     * @Route("/settings", name="site_account_settings")
     * @return Response|array
     */
    public function settingsAction()
    {
        return [];
    }

    /**
     * @Route("/ask-friends", name="site_account_ask_friends", methods={"POST"})
     * @return Response
     */
    public function askFriends(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $currentUser = $this->getUser();
        foreach ($this->getFollowRepository()->getUserFollows($currentUser) as $user) {
            if ($this->isGranted(UserVoter::ASK, $user)) {
                $question = (new Question())
                    ->setText($request->get('text'))
                    ->setSender($currentUser)
                    ->setReceiver($user);
                $em->persist($question);
            }
        }
        $em->flush();
        return $this->json([]);
    }

}
