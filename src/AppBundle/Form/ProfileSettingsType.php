<?php

namespace AppBundle\Form;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\{
    BirthdayType, CheckboxType, ChoiceType, EmailType, HiddenType, TextareaType, TextType, UrlType
};
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileSettingsType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('theme_id', HiddenType::class, ['property_path' => 'theme'])
            ->add('emoodji_id', HiddenType::class, ['property_path' => 'emoodji'])
            ->add('fullName', TextType::class, ['label' => 'form.user.full_name'])
            ->add('location', TextType::class, ['label' => 'form.user.location'])
            ->add('biography', TextareaType::class, ['label' => 'form.user.bio'])
            ->add('website', UrlType::class, ['label' => 'form.user.website'])
            //Tags
            ->add('username', TextType::class, ['disabled' => true, 'label' => 'form.user.username'])
            ->add('email', EmailType::class, ['disabled' => true, 'label'=> 'form.user.email'])
            ->add('bornAt', BirthdayType::class, [
                'label' => 'form.user.birthday',
                'required' => true,
                'format' => 'dd-MMMM-yyyy',
                'placeholder' => [
                    'year' => 'form.user.year',
                    'month' => 'form.user.month',
                    'day' => 'form.user.day',
                ],
                'years' => range(date('Y'), date('Y')-100)
            ])
            ->add('gender', ChoiceType::class, [
                'label' => 'form.user.my_gender',
                'required' => false,
                'attr'=> ['class' => 'simpleFormItem'],
                'choices' => [
                    'empty' => '',
                    'form.user.female' => 1,
                    'form.user.male' => 2
                ]
            ])
            ->add('allowAnonymousQuestions', CheckboxType::class, ['label' => 'form.user.allow_anonymous_questions'])
            ->add('allowAnswerSharing', CheckboxType::class, ['label' => 'form.user.allow_answer_sharing'])
            ->add('allowSubscribing', CheckboxType::class, ['label' => 'form.user.allow_subscribing'])
            ->add('allowDiscovery', CheckboxType::class, ['label' => 'form.user.allow_discovery'])
            ->add('showOnlineStatus', CheckboxType::class, ['label' => 'form.user.show_online_status'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'method' => 'put',
            'allow_extra_fields' => true,
            'attr' => [
                'id' => 'profileForm',
                'autocomplete' => 'off',
                'class' => 'edit_user',
                'data-action' => 'SubmitWithTimezone',
                'accept-charset' => 'UTF-8',
            ]
        ]);
    }

    public function getBlockPrefix()
    {
        return 'user';
    }

}
