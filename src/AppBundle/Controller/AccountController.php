<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Question;
use AppBundle\Security\UserVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/account")
 * @Template()
 * @Security("has_role('ROLE_USER')")
 */
class AccountController extends AbstractController
{

    /**
     * @Route("/wall", name="site_account_wall")
     * @return Response|array
     */
    public function wallAction()
    {
//        $this->get('old_sound_rabbit_mq.test_producer')
//            ->publish(json_encode(['name' => 'Denchik1234']));
        $eventRepository = $this->getEventRepository();
        $followRepository = $this->getFollowRepository();
        return [
            'events' => $eventRepository->getFeed($this->getUser()),
            'follows' => $followRepository->getUserLastFollows($this->getUser())
        ];
    }

    /**
     * @Route("/inbox", name="site_account_inbox")
     * @return Response|array
     */
    public function inboxAction()
    {
        $questionRepository = $this->getQuestionRepository();
        $questions = $questionRepository->getNotAnswered($this->getUser());
        return ['questions' => $questions];
    }

    /**
     * @Route("/friends", name="site_account_friends")
     * @return Response|array
     */
    public function friendsAction()
    {
        return ['friends' => $this->getFollowRepository()->getUserFollows($this->getUser())];
    }

    /**
     * @Route("/notifications", name="site_account_notifications")
     * @return Response|array
     */
    public function notificationsAction()
    {
        return [];
    }

    /**
     * @Route("/ask-friends", name="site_account_ask_friends_modal", methods={"PUT"})
     * @param Request $request
     * @return Response|array
     */
    public function askFriendsModalAction(Request $request)
    {
        $questionText = $request->get('question_text');
        $isAnonymous = (bool)$request->get('anonymous');
        return [
            'questionText' => $questionText,
            'isAnonymous' => $isAnonymous,
            'friends' => $this->getFollowRepository()->getUserFollows($this->getUser())
        ];
    }

    /**
     * @Route("/ask-friends", name="site_account_ask_friends", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function askFriendsAction(Request $request): Response
    {
        $questionText = $request->get('question_text');
        $isAnonymous = (bool)$request->get('anonymous');
        $length = strlen($questionText);
        if ($length === 0 || $length > 300) {
            return $this->jsonErrors('Вы забыли заполнить некоторые важные поля', ['question_text']);
        }
        $requestQuestion = $request->get('question', ['logins' => []]);
        $users = $this->getUserRepository()->getUsersByUserNames($requestQuestion['logins']);
        $em = $this->getDoctrine()->getManager();
        $currentUser = $this->getUser();
        foreach ($users as $user) {
            if ($this->isGranted(UserVoter::ASK, $user)) {
                $question = (new Question())
                    ->setText($questionText)
                    ->setSender($isAnonymous ? null : $currentUser)
                    ->setReceiver($user);
                $em->persist($question);
            }
        }
        $em->flush();
        return $this->jsonLastUrl();
    }

}
