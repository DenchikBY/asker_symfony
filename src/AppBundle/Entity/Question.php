<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="questions")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuestionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Question
{

    use BaseEntity;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="from_user_id", onDelete="SET NULL")
     * @var User
     */
    private $sender;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="to_user_id", onDelete="CASCADE")
     * @var User
     */
    private $receiver;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max="300")
     * @ORM\Column(type="string")
     * @var string
     */
    private $text;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    private $isAnswered = false;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    private $answerText;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $answeredAt;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $likesCount = 0;

    /**
     * @return User
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param User $sender
     * @return $this
     */
    public function setSender(User $sender)
    {
        $this->sender = $sender;
        return $this;
    }

    /**
     * @return User
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * @param User $receiver
     * @return $this
     */
    public function setReceiver(User $receiver)
    {
        $this->receiver = $receiver;
        return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return $this
     */
    public function setText(string $text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsAnswered()
    {
        return $this->isAnswered;
    }

    /**
     * @param bool $isAnswered
     * @return $this
     */
    public function setIsAnswered(bool $isAnswered)
    {
        $this->isAnswered = $isAnswered;
        return $this;
    }

    /**
     * @return string
     */
    public function getAnswerText()
    {
        return $this->answerText;
    }

    /**
     * @param string $answerText
     * @return $this
     */
    public function setAnswerText(string $answerText)
    {
        $this->answerText = $answerText;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getAnsweredAt()
    {
        return $this->answeredAt;
    }

    /**
     * @param \DateTime $answeredAt
     * @return $this
     */
    public function setAnsweredAt(\DateTime $answeredAt)
    {
        $this->answeredAt = $answeredAt;
        return $this;
    }

    /**
     * @return int
     */
    public function getLikesCount()
    {
        return $this->likesCount;
    }

    /**
     * @param int $likesCount
     * @return $this
     */
    public function setLikesCount(int $likesCount)
    {
        $this->likesCount = $likesCount;
        return $this;
    }

}
