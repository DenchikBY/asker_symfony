<?php

namespace AppBundle\Consumer;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

class TestConsumer implements ConsumerInterface
{

    /** @var EntityManager */
    private $entityManager;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(EntityManager $entityManager, LoggerInterface $logger)
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    /**
     * @param AMQPMessage $msg The message
     * @return mixed false to reject and requeue, any other value to acknowledge
     */
    public function execute(AMQPMessage $msg)
    {
        try {
            $message = json_decode($msg->body);
            $user = (new User())
                ->setUsername($message->name)
                ->setEmail("{$message->name}@{$message->name}.com")
                ->setPassword($message->name);
            //$userRepo = $this->entityManager->getRepository('AppBundle:User');
            //$user = $userRepo->find(1)->setUsername($message->name);
            //$userRepo->save($user);
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            $this->logger->info('Success!');
        } catch (Exception $exception) {
            $this->logger->error('Error: ' . $exception->getMessage());
        }
        return true;
    }

}
