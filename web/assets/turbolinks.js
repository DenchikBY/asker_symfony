let jquery = $;
(function() {
    var x, H, k, g, y, I, t, J, u, K, z, v, A, B, L, M, N, q, O, P, Q, C, D, R, S, T, U, n, V, l, W, X, f, Y, Z, w, $, aa, E, F, ba, ca, da, ea, fa, G, ga, h, r, d, ha = [].indexOf || function(a) {
                for (var b = 0, c = this.length; b < c; b++) if (b in this && this[b] === a) return b;
                return -1
            }, ja = function(a, b) {
            function c() {
                this.constructor = a
            }
            for (var e in b) ia.call(b, e) && (a[e] = b[e]);
            c.prototype = b.prototype;
            a.prototype = new c;
            a.__super__ = b.prototype;
            return a
        }, ia = {}.hasOwnProperty,
        ka = [].slice,
        la = function(a, b) {
            return function() {
                return a.apply(b, arguments)
            }
        };
    l = {};
    v = 10;
    G = !1;
    d = w = n = q = f = null;
    g = {
        BEFORE_CHANGE: "page:before-change",
        FETCH: "page:fetch",
        RECEIVE: "page:receive",
        CHANGE: "page:change",
        UPDATE: "page:update",
        LOAD: "page:load",
        RESTORE: "page:restore",
        BEFORE_UNLOAD: "page:before-unload",
        EXPIRE: "page:expire"
    };
    Q = function(a) {
        var b;
        a = new k(a);
        ba();
        z();
        null !== f && f.start();
        return G && (b = ga(a.absolute)) ? (C(b), D(a, null, !1)) : D(a, ea)
    };
    ga = function(a) {
        if ((a = l[a]) && !a.transitionCacheDisabled) return a
    };
    D = function(a, b, c) {
        null === c && (c = !0);
        h(g.FETCH, {
            url: a.absolute
        });
        null !== d && d.abort();
        d = new XMLHttpRequest;
        d.open("GET", a.withoutHashForIE10compatibility(), !0);
        d.setRequestHeader("Accept", "text/html, application/xhtml+xml, application/xml");
        d.setRequestHeader("X-XHR-Referer", w);
        d.onload = function() {
            var c;
            h(g.RECEIVE, {
                url: a.absolute
            });
            return (c = X()) ? ($(a), aa(), A.apply(null, P(c)), V(), "function" === typeof b && b(), h(g.LOAD)) : document.location.href = N() || a.absolute
        };
        f && c && (d.onprogress = function(a) {
            return function(a) {
                return f.advanceTo(a.lengthComputable ? a.loaded / a.total * 100 : f.value + (100 - f.value) / 10)
            }
        }(this));
        d.onloadend = function() {
            return d = null
        };
        d.onerror = () => document.location.href = a.absolute;
        return d.send()
    };
    C = function(a) {
        null != d && d.abort();
        A(a.title, a.body);
        Y(a);
        return h(g.RESTORE)
    };
    z = function() {
        var a;
        a = new k(q.url);
        l[a.absolute] = {
            url: a.relative,
            body: document.body,
            title: document.title,
            positionY: window.pageYOffset,
            positionX: window.pageXOffset,
            cachedAt: (new Date).getTime(),
            transitionCacheDisabled: null != document.querySelector("[data-no-transition-cache]")
        };
        return L(v)
    };
    L = function(a) {
        var b,
            c, e, s, d, p;
        d = Object.keys(l);
        b = d.map(function(a) {
            return l[a].cachedAt
        }).sort(function(a, b) {
            return b - a
        });
        p = [];
        c = 0;
        for (s = d.length; c < s; c++) e = d[c], l[e].cachedAt <= b[a] && (h(g.EXPIRE, l[e]), p.push(delete l[e]));
        return p
    };
    A = function(a, b, c, e) {
        h(g.BEFORE_UNLOAD);
        document.title = a;
        document.documentElement.replaceChild(b, document.body);
        null != c && x.update(c);
        fa();
        e && O();
        q = window.history.state;
        null != f && f.done();
        h(g.CHANGE);
        return h(g.UPDATE)
    };
    O = function() {
        var a, b, c, e, s, d, p, f, m, g;
        g = Array.prototype.slice.call(document.body.querySelectorAll('script:not([data-turbolinks-eval="false"])'));
        c = 0;
        for (s = g.length; c < s; c++) if (m = g[c], "" === (p = m.type) || "text/javascript" === p) {
            b = document.createElement("script");
            f = m.attributes;
            e = 0;
            for (d = f.length; e < d; e++) a = f[e], b.setAttribute(a.name, a.value);
            m.hasAttribute("async") || (b.async = !1);
            b.appendChild(document.createTextNode(m.innerHTML));
            e = m.parentNode;
            a = m.nextSibling;
            e.removeChild(m);
            e.insertBefore(b, a)
        }
    };
    ca = function(a) {
        a.innerHTML = a.innerHTML.replace(/<noscript[\S\s]*?<\/noscript>/ig, "");
        return a
    };
    fa = function() {
        var a, b;
        if ((a = (b = document.querySelectorAll("input[autofocus], textarea[autofocus]"))[b.length - 1]) && document.activeElement !== a) return a.focus()
    };
    $ = function(a) {
        if ((a = new k(a)).absolute !== w) return window.history.pushState({
            turbolinks: !0,
            url: a.absolute
        }, "", a.absolute)
    };
    aa = function() {
        var a, b;
        if (a = d.getResponseHeader("X-XHR-Redirected-To")) return a = new k(a), b = a.hasNoHash() ? document.location.hash : "", window.history.replaceState(window.history.state, "", a.href + b)
    };
    N = function() {
        var a;
        if (null != (a = d.getResponseHeader("Location")) && (new k(a)).crossOrigin()) return a
    };
    ba = function() {
        return w = document.location.href
    };
    F = function() {
        return window.history.replaceState({
            turbolinks: !0,
            url: document.location.href
        }, "", document.location.href)
    };
    E = function() {
        return q = window.history.state
    };
    V = function() {
        var a;
        if (navigator.userAgent.match(/Firefox/) && !(a = new k).hasNoHash()) return window.history.replaceState(q, "", a.withoutHash()), document.location.hash = a.hash
    };
    Y = function(a) {
        return window.scrollTo(a.positionX, a.positionY)
    };
    ea = function() {
        return document.location.hash ? document.location.href = document.location.href : window.scrollTo(0, 0)
    };
    B = function(a) {
        var b, c, e;
        if (null == a || "object" !== typeof a) return a;
        b = new a.constructor;
        for (c in a) e = a[c], b[c] = B(e);
        return b
    };
    h = function(a, b) {
        var c;
        "undefined" !== typeof Prototype && Event.fire(document, a, b, !0);
        c = document.createEvent("Events");
        b && (c.data = b);
        c.initEvent(a, !0, !0);
        return document.dispatchEvent(c)
    };
    W = function(a) {
        return !h(g.BEFORE_CHANGE, {
            url: a
        })
    };
    X = function() {
        var a, b, c, e;
        b = function() {
            var a;
            return null != (a = d.getResponseHeader("Content-Type")) && a.match(/^(?:text\/html|application\/xhtml\+xml|application\/xml)(?:;|$)/)
        };
        c = function(a) {
            var b, c, e, d;
            e = a.querySelector("head").childNodes;
            d = [];
            a = 0;
            for (b = e.length; a < b; a++) c = e[a], null != ("function" === typeof c.getAttribute ? c.getAttribute("data-turbolinks-track") : void 0) && d.push(c.getAttribute("src") || c.getAttribute("href"));
            return d
        };
        a = function(a) {
            n || (n = c(document));
            a = c(a);
            return a.length !== n.length || e(a, n).length !== n.length
        };
        e = function(a, b) {
            var c, e, d, f;
            a.length > b.length && (c = [b, a], a = c[0], b = c[1]);
            d = [];
            c = 0;
            for (e = a.length; c < e; c++) f = a[c], 0 <= ha.call(b, f) && d.push(f);
            return d
        };
        if (! function() {
                var a;
                return 400 <= (a = d.status) && 600 > a
            }() && b() && (b = M(d.responseText)) && !a(b)) return b
    };
    P = function(a) {
        var b;
        b = a.querySelector("title");
        return [null != b ? b.textContent : void 0, ca(a.querySelector("body")), x.get(a).token, "runScripts"]
    };
    x = {
        get: function(a) {
            var b;
            null == a && (a = document);
            return {
                node: b = a.querySelector('meta[name="csrf-token"]'),
                token: null != b ? "function" === typeof b.getAttribute ? b.getAttribute("content") : void 0 : void 0
            }
        },
        update: function(a) {
            var b;
            b = this.get();
            if (null != b.token && null != a && b.token !== a) return b.node.setAttribute("content",
                a)
        }
    };
    M = function(a) {
        var b;
        b = document.documentElement.cloneNode();
        b.innerHTML = a;
        b.head = b.querySelector("head");
        b.body = b.querySelector("body");
        return b
    };
    k = function() {
        function a(b) {
            this.original = null != b ? b : document.location.href;
            if (this.original.constructor === a) return this.original;
            this._parse()
        }
        a.prototype.withoutHash = function() {
            return this.href.replace(this.hash, "").replace("#", "")
        };
        a.prototype.withoutHashForIE10compatibility = function() {
            return this.withoutHash()
        };
        a.prototype.hasNoHash = function() {
            return 0 === this.hash.length
        };
        a.prototype.crossOrigin = function() {
            return this.origin !== (new a).origin
        };
        a.prototype._parse = function() {
            var a;
            (null != this.link ? this.link : this.link = document.createElement("a")).href = this.original;
            a = this.link;
            this.href = a.href;
            this.protocol = a.protocol;
            this.host = a.host;
            this.hostname = a.hostname;
            this.port = a.port;
            this.pathname = a.pathname;
            this.search = a.search;
            this.hash = a.hash;
            this.origin = [this.protocol, "//", this.hostname].join("");
            0 !== this.port.length && (this.origin += ":" + this.port);
            this.relative = [this.pathname, this.search, this.hash].join("");
            return this.absolute = this.href
        };
        return a
    }();
    y = function(a) {
        function b(a) {
            this.link = a;
            if (this.link.constructor === b) return this.link;
            this.original = this.link.href;
            this.originalElement = this.link;
            this.link = this.link.cloneNode(!1);
            b.__super__.constructor.apply(this, arguments)
        }
        ja(b, a);
        b.HTML_EXTENSIONS = ["html"];
        b.allowExtensions = function() {
            var a, e, d, f;
            e = 1 <= arguments.length ? ka.call(arguments, 0) : [];
            d = 0;
            for (f = e.length; d < f; d++) a = e[d], b.HTML_EXTENSIONS.push(a);
            return b.HTML_EXTENSIONS
        };
        b.prototype.shouldIgnore = function() {
            return this.crossOrigin() || this._anchored() || this._nonHtml() || this._optOut() || this._target()
        };
        b.prototype._anchored = function() {
            return (0 < this.hash.length || "#" === this.href.charAt(this.href.length - 1)) && this.withoutHash() === (new k).withoutHash()
        };
        b.prototype._nonHtml = function() {
            return this.pathname.match(/\.[a-z]+$/g) && !this.pathname.match(new RegExp("\\.(?:" + b.HTML_EXTENSIONS.join("|") + ")?$", "g"))
        };
        b.prototype._optOut = function() {
            var a, b;
            for (b = this.originalElement; !a && b !== document;) a = null != b.getAttribute("data-no-turbolink"), b = b.parentNode;
            return a
        };
        b.prototype._target = function() {
            return 0 !== this.link.target.length
        };
        return b
    }(k);
    H = function() {
        function a(a) {
            this.event = a;
            this.event.defaultPrevented || (this._extractLink(), this._validForTurbolinks() && (W(this.link.absolute) || r(this.link.href), this.event.preventDefault()))
        }
        a.installHandlerLast = function(b) {
            if (!b.defaultPrevented) return document.removeEventListener("click", a.handle, !1), document.addEventListener("click", a.handle, !1)
        };
        a.handle = function(b) {
            return new a(b)
        };
        a.prototype._extractLink = function() {
            var a;
            for (a = this.event.target; a.parentNode && "A" !== a.nodeName;) a = a.parentNode;
            if ("A" === a.nodeName && 0 !== a.href.length) return this.link = new y(a)
        };
        a.prototype._validForTurbolinks = function() {
            return null != this.link && !(this.link.shouldIgnore() || this._nonStandardClick())
        };
        a.prototype._nonStandardClick = function() {
            return 1 < this.event.which || this.event.metaKey || this.event.ctrlKey || this.event.shiftKey || this.event.altKey
        };
        return a
    }();
    I = function() {
        function a(a) {
            this.elementSelector = a;
            this._trickle = la(this._trickle, this);
            this.value = 0;
            this.content = "";
            this.speed = 300;
            this.opacity = .99;
            this.install()
        }
        a.prototype.install = function() {
            this.element = document.querySelector(this.elementSelector);
            this.element.classList.add("turbolinks-progress-bar");
            this.styleElement = document.createElement("style");
            document.head.appendChild(this.styleElement);
            return this._updateStyle()
        };
        a.prototype.uninstall = function() {
            this.element.classList.remove("turbolinks-progress-bar");
            return document.head.removeChild(this.styleElement)
        };
        a.prototype.start = function() {
            return this.advanceTo(5)
        };
        a.prototype.advanceTo = function(a) {
            var c;
            if (a > (c = this.value) && 100 >= c) {
                this.value = a;
                this._updateStyle();
                if (100 === this.value) return this._stopTrickle();
                if (0 < this.value) return this._startTrickle()
            }
        };
        a.prototype.done = function() {
            if (0 < this.value) return this.advanceTo(100), this._reset()
        };
        a.prototype._reset = function() {
            var a;
            a = this.opacity;
            setTimeout(function(a) {
                    return function() {
                        a.opacity = 0;
                        return a._updateStyle()
                    }
                }(this),
                this.speed / 2);
            return setTimeout(function(c) {
                return function() {
                    c.value = 0;
                    c.opacity = a;
                    return c._withSpeed(0, function() {
                        return c._updateStyle(!0)
                    })
                }
            }(this), this.speed)
        };
        a.prototype._startTrickle = function() {
            if (!this.trickling) return this.trickling = !0, setTimeout(this._trickle, this.speed)
        };
        a.prototype._stopTrickle = function() {
            return delete this.trickling
        };
        a.prototype._trickle = function() {
            if (this.trickling) return this.advanceTo(this.value + Math.random() / 2), setTimeout(this._trickle, this.speed)
        };
        a.prototype._withSpeed = function(a, c) {
            var d, f;
            d = this.speed;
            this.speed = a;
            f = c();
            this.speed = d;
            return f
        };
        a.prototype._updateStyle = function(a) {
            null == a && (a = !1);
            a && this._changeContentToForceRepaint();
            return this.styleElement.textContent = this._createCSSRule()
        };
        a.prototype._changeContentToForceRepaint = function() {
            return this.content = "" === this.content ? " " : ""
        };
        a.prototype._createCSSRule = function() {
            return this.elementSelector + ".turbolinks-progress-bar::before {\n  content: '" + this.content + "';\n  position: fixed;\n  top: 0;\n  left: 0;\n  z-index: 2000;\n  background-color: #0076ff;\n  height: 3px;\n  opacity: " + this.opacity + ";\n  width: " + this.value + "%;\n  transition: width " + this.speed + "ms ease-out, opacity " + this.speed / 2 + "ms ease-in;\n  transform: translate3d(0,0,0);\n}"
        };
        return a
    }();
    K = function(a) {
        return setTimeout(a, 500)
    };
    S = function() {
        return document.addEventListener("DOMContentLoaded", function() {
            h(g.CHANGE);
            return h(g.UPDATE)
        }, !0)
    };
    U = function() {
        if ("undefined" !== typeof jQuery) return jQuery(document).on("ajaxSuccess", function(a, b, c) {
            if (jQuery.trim(b.responseText)) return h(g.UPDATE)
        })
    };
    T = function(a) {
        var b;
        if (null != (b = a.state) && b.turbolinks) return (b = l[(new k(a.state.url)).absolute]) ? (z(), C(b)) : r(a.target.location.href)
    };
    R = function() {
        F();
        E();
        document.addEventListener("click", H.installHandlerLast, !0);
        window.addEventListener("hashchange", function(a) {
            F();
            return E()
        }, !1);
        return K(function() {
            return window.addEventListener("popstate", T, !1)
        })
    };
    t = void 0 !== window.history.state || navigator.userAgent.match(/Firefox\/2[6|7]/);
    J = window.history && window.history.pushState && window.history.replaceState && t;
    t = !navigator.userAgent.match(/CriOS\//);
    da = "GET" === (Z = function(a) {
            var b, c;
            c = (null != (b = document.cookie.match(new RegExp(a + "=(\\w+)"))) ? b[1].toUpperCase() : void 0) || "";
            document.cookie = a + "=; expires=Thu, 01-Jan-70 00:00:01 GMT; path=/";
            return c
        }("request_method")) || "" === Z;
    u = J && t && da;
    document.addEventListener && document.createEvent && (S(), U());
    u ? (r = Q, R()) : r = function(a) {
            return document.location.href = a
        };
    this.Turbolinks = {
        visit: r,
        pagesCached: function(a) {
            null == a && (a = v);
            if (/^[\d]+$/.test(a)) return v = parseInt(a)
        },
        enableTransitionCache: function(a) {
            null == a && (a = !0);
            return G = a
        },
        enableProgressBar: function(a) {
            null == a && (a = !0);
            if (u) {
                if (a) return null != f ? f : f = new I("html");
                null != f && f.uninstall();
                return f = null
            }
        },
        allowLinkExtensions: y.allowExtensions,
        supported: u,
        EVENTS: B(g)
    }
}).call(this);