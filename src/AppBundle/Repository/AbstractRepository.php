<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

abstract class AbstractRepository extends EntityRepository
{

    public function save($entity)
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
    }

    public function update($entity)
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush($entity);
    }

    public function delete($entity)
    {
        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();
    }

    public function persist($entity)
    {
        $this->getEntityManager()->persist($entity);
    }

    public function clear()
    {
        $this->getEntityManager()->clear();
    }

    public function disabledLogger()
    {
        $this->getEntityManager()->getConnection()->getConfiguration()->setSQLLogger(null);
    }

}
