<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\User;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\{
    JsonResponse, RedirectResponse, RequestStack, Response
};
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;
use Symfony\Component\Translation\TranslatorInterface;

class AuthenticationListener implements EventSubscriberInterface
{

    /** @var EventDispatcherInterface */
    private $dispatcher;

    /** @var RequestStack */
    private $requestStack;

    /** @var TranslatorInterface */
    private $translator;

    /** @var UrlGeneratorInterface */
    private $urlGenerator;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var AccessDecisionManagerInterface */
    private $decisionManager;

    /** @var Response */
    private $response;

    public function __construct(EventDispatcherInterface $dispatcher, RequestStack $requestStack,
                                TranslatorInterface $translator, UrlGeneratorInterface $urlGenerator,
                                TokenStorageInterface $tokenStorage, AccessDecisionManagerInterface $decisionManager)
    {
        $this->dispatcher = $dispatcher;
        $this->requestStack = $requestStack;
        $this->translator = $translator;
        $this->urlGenerator = $urlGenerator;
        $this->tokenStorage = $tokenStorage;
        $this->decisionManager = $decisionManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            AuthenticationEvents::AUTHENTICATION_FAILURE => 'onAuthenticationFailure',
            SecurityEvents::INTERACTIVE_LOGIN => 'onAuthenticationSuccess',
            FOSUserEvents::REGISTRATION_INITIALIZE => 'onRegistrationInitialize',
            FOSUserEvents::REGISTRATION_FAILURE => 'onRegistrationFailure',
            FOSUserEvents::REGISTRATION_SUCCESS => 'onRegistrationSuccess',
        ];
    }

    public function onAuthenticationSuccess(InteractiveLoginEvent $event)
    {
        $this->response = new JsonResponse(['url' => $this->urlGenerator->generate('site_account_wall')]);
        $this->dispatcher->addListener(KernelEvents::RESPONSE, [$this, 'returnResponse']);
    }

    public function onAuthenticationFailure(AuthenticationFailureEvent $event)
    {
        $fields = [];
        $request = $this->requestStack->getCurrentRequest();
        $username = $request->get('_username');
        $password = $request->get('_password');
        if ($username == null || $password == null) {
            $error = $this->translator->trans('form.login.errors.empty');
            if ($username == null) $fields[] = '_username';
            if ($password == null) $fields[] = '_password';
        } else {
            $error = $this->translator->trans('form.login.errors.bad_credentials');
            $fields = ['_username', '_password'];
        }
        $this->response = new JsonResponse([
            'error' => $error,
            'fields' => $fields
        ]);
        $this->dispatcher->addListener(KernelEvents::RESPONSE, [$this, 'returnResponse']);
    }

    public function onRegistrationInitialize(UserEvent $event)
    {
        if ($this->decisionManager->decide($this->tokenStorage->getToken(), [User::ROLE_USER])) {
            $this->response = new RedirectResponse($this->urlGenerator->generate('site_account_wall'));
            $this->dispatcher->addListener(KernelEvents::RESPONSE, [$this, 'returnResponse']);
        }
    }

    public function onRegistrationFailure(FormEvent $event)
    {
        $fields = [];
        $errors = $event->getForm()->getErrors(true);
        $message = $errors->current()->getMessage();
        foreach ($event->getForm()->all() as $field) {
            if (!$field->isValid())
                $fields[] = "{$event->getForm()->getName()}[{$field->getName()}]";
        }
        $response = new JsonResponse([
            'error' => $message,
            'fields' => $fields
        ]);
        $event->setResponse($response);
    }

    public function onRegistrationSuccess(FormEvent $event)
    {
        $response = new JsonResponse(['url' => $this->urlGenerator->generate('site_account_wall')]);
        $event->setResponse($response);
    }

    public function returnResponse(FilterResponseEvent $event)
    {
        $event->setResponse($this->response);
    }

}
