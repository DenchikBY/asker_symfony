<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Follow;
use AppBundle\Entity\User;
use Doctrine\ORM\Query;

class FollowRepository extends AbstractRepository
{

    /**
     * @param User $follower
     * @param User $followed
     * @return bool
     */
    public function follow(User $follower, User $followed): bool
    {
        if ($follower->getId() == $followed->getId())
            return false;
        $follow = $this->getFollow($follower, $followed);
        if ($follow == null) {
            $follow = (new Follow())
                ->setFollower($follower)
                ->setFollowed($followed);
            $this->save($follow);
            $followed->setFollowersCount($followed->getFollowersCount() + 1);
            $this->getEntityManager()->persist($followed);
            $this->getEntityManager()->flush();
        }
        return true;
    }

    /**
     * @param User $follower
     * @param User $followed
     * @return bool
     */
    public function unFollow(User $follower, User $followed): bool
    {
        if ($follower->getId() == $followed->getId())
            return false;
        $follow = $this->getFollow($follower, $followed);
        if ($follow != null) {
            $this->delete($follow);
            $followed->setFollowersCount($followed->getFollowersCount() - 1);
            $this->getEntityManager()->persist($followed);
            $this->getEntityManager()->flush();
        }
        return true;
    }

    /**
     * @param User $follower
     * @param User $followed
     * @return mixed
     */
    public function getFollow(User $follower, User $followed)
    {
        return $this->createQueryBuilder('f')
            ->where('f.follower = :follower')
            ->andWhere('f.followed = :followed')
            ->setParameters([
                'follower' => $follower,
                'followed' => $followed
            ])
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param User $user
     * @return User[]|null
     */
    public function getUserFollows(User $user)
    {
        return $this->getEntityManager()
            ->createQuery('select u from AppBundle:User u where u in (select IDENTITY(f.followed) from AppBundle:Follow f where f.follower = :user)')
            ->setParameter('user', $user)
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getResult();
    }

    /**
     * @param User $user
     * @return User[]|null
     */
    public function getUserLastFollows(User $user)
    {
        return $this->getEntityManager()
            ->createQuery('select u from AppBundle:User u where u in (select IDENTITY(f.followed) from AppBundle:Follow f where f.follower = :user)')
            ->setParameter('user', $user)
            ->setMaxResults(User::SIDE_MENU_FOLLOWS_COUNT)
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            //->useResultCache(true, 3600, 'db_user_' . $user->getId() . '_last_follows')
            ->getResult();
    }

}
