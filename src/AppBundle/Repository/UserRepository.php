<?php

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Doctrine\ORM\Query;

class UserRepository extends AbstractRepository
{

    /**
     * @param string $username
     * @param User|null $viewer
     * @return User|null
     */
    public function findByUsername(string $username, User $viewer = null)
    {
        $result = $this->getEntityManager()
            ->createQuery('select u, (select count(f.id) from AppBundle\Entity\Follow f where f.follower = :viewer and f.followed = u) as followedByViewer from AppBundle\Entity\User u where u.username = :username')
            ->setParameter('viewer', $viewer)
            ->setParameter('username', $username)
            ->setMaxResults(1)
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getOneOrNullResult();
        $user = $result[0];
        if ($user == null) return null;
        return $user->setFollowedByViewer((bool)$result['followedByViewer']);
    }

    /**
     * @param int $limit
     * @return User[]|null
     */
    public function getRandomUsers(int $limit = 10)
    {
        return $this->createQueryBuilder('u')
            ->setMaxResults($limit)
            ->orderBy('u.createdAt', 'desc')
            ->getQuery()
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->useResultCache(true, 3600, 'homepage_random_users')
            ->getResult();
    }

    /**
     * @param string[] $userNames
     * @return User[]|null
     */
    public function getUsersByUserNames(array $userNames)
    {
        $qb = $this->createQueryBuilder('u');
        return $qb->where($qb->expr()->in('u.username', $userNames))
            ->getQuery()
            ->getResult();
    }

}
