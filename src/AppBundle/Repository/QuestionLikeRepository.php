<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Question;
use AppBundle\Entity\QuestionLike;
use AppBundle\Entity\User;

class QuestionLikeRepository extends AbstractRepository
{

    /**
     * @param User $user
     * @param Question $question
     * @return bool
     */
    public function like(User $user, Question $question): bool
    {
        if ($question->getReceiver()->getId() == $user->getId())
            return false;
        $like = $this->getLike($user, $question);
        if ($like == null) {
            $like = (new QuestionLike())
                ->setUser($user)
                ->setQuestion($question);
            $this->save($like);
            $question->setLikesCount($question->getLikesCount() + 1);
            $this->getEntityManager()->persist($question);
            $this->getEntityManager()->flush();
        }
        return true;
    }

    /**
     * @param User $user
     * @param Question $question
     * @return bool
     */
    public function dislike(User $user, Question $question): bool
    {
        if ($question->getReceiver()->getId() == $user->getId())
            return false;
        $like =  $this->getLike($user, $question);
        if ($like != null) {
            $this->delete($like);
            $question->setLikesCount($question->getLikesCount() - 1);
            $this->getEntityManager()->persist($question);
            $this->getEntityManager()->flush();
        }
        return true;
    }

    /**
     * @param User $user
     * @param Question $question
     * @return mixed
     */
    public function getLike(User $user, Question $question)
    {
        return $this->createQueryBuilder('l')
            ->where('l.user = :user')
            ->andWhere('l.question = :question')
            ->setParameters([
                'user' => $user,
                'question' => $question
            ])
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

}
