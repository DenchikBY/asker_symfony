
(function(b) {
    function d(b, a, c) {
        this.leftRegex = new RegExp((b || /(?:^|\s)(\S+)/).source + "$");
        this.rightRegex = new RegExp("^" + (a || /(\S*)/).source);
        this.maxLength = 0 < c ? c : 128
    }
    d.VERSION = "0.2";
    d.startsWith = function(b) {
        return new d(new RegExp(["(?:^|\\s)(", b, "[^", b, "\\s]*)"].join("")))
    };
    d.prototype.token = function(b, a) {
        var c;
        "string" === typeof b ? c = b : (c = b.value, a = b.selectionStart);
        var f = c.slice(0, a);
        c = c.slice(a);
        var g = f.slice(-this.maxLength),
            f = c.slice(0, this.maxLength),
            g = (c = g.match(this.leftRegex)) ? c[1] : "",
            f = (c = f.match(this.rightRegex)) ? c[1] : "";
        return {
            value: g ? g + f : "",
            prefix: g,
            suffix: g ? f : "",
            toString: function() {
                return this.value
            }
        }
    };
    d.prototype.replace = function(b, a, c) {
        var f;
        "string" === typeof b ? f = b : (f = b.value, c = a, a = b.selectionStart);
        var g = this.token(f, a);
        if (f && !g.value) return "string" === typeof b ? f : !1;
        var h = f.slice(0, a);
        f = f.slice(a);
        h = [h.slice(0, h.length - g.prefix.length), c, f.slice(g.suffix.length)].join("");
        if ("string" === typeof b) return h;
        c = c.length - g.prefix.length;
        b.value = h;
        b.setSelectionRange(a + c, a + c);
        return !0
    };
    b.Cursores = d
})(this);
(function(b, d) {
    b.prototype.find === d && (b.prototype.find = function(b) {
        for (var a = 0; a < this.length; ++a) if (b(this[a])) return this[a];
        return null
    })
})(Array);
(function(b, d) {
    b.prototype.join === d && (b.prototype.join = function(b) {
        for (var a = "", c = 0; c < this.length; ++c) 0 < c && (a += b), a += this[c];
        return a
    })
})(Array);
(function(b, d) {
    b.prototype.map === d && (b.prototype.map = function(b) {
        for (var a = [], c = 0; c < this.length; ++c) a[c] = b(this[c]);
        return a
    })
})(Array);
(function(b, d) {
    b.prototype.toObject === d && (b.prototype.toObject = function() {
        for (var b = {}, a = 0; a < this.length; a += 2) this[a] !== d && (b[this[a]] = this[a + 1]);
        return b
    })
})(Array);
jQuery.fn.bg = function(b) {
    return this.css("background-image", "url(" + b + ")")
};
jQuery.json = function(b, d, e) {
    d.dataType = "json";
    return this.ajax(b, d).done(function(b, c, f) {
        return e.call(this, f.responseJSON)
    }).fail(function(b, c, f) {
        return e.call(this, b.responseJSON)
    })
};
jQuery.fn.presence = function() {
    return 0 < this.length ? this : null
};
Utils = function(b, d, e) {
    function a(c) {
        return 0 < b.trim(c).length
    }
    return {
        getCsrfToken: function() {
            return b('meta[name="csrf-token"]').attr("content")
        },
        debounce: function(b, f) {
            var a;
            return function() {
                a && d.clearTimeout(a);
                var h = this,
                    e = arguments;
                a = d.setTimeout(function() {
                    b.apply(h, e);
                    a = null
                }, f)
            }
        },
        serialize: function() {
            var c = {
                INPUT: function(b) {
                    return [b.attr("name"), b.val()].toObject()
                }
            };
            return function(f) {
                var a = c[f.prop("tagName")],
                    a = a && a(f) || {};
                return b.extend(a, f.data("params"))
            }
        }(),
        $tag$: function(c) {
            if ("undefined" === typeof c.tagName) throw Error("Target element must be a tag.");
            return b(c)
        },
        isBlank: function(b) {
            return !a(b)
        },
        isPresent: a,
        cropFields: function(c, a) {
            b.each(a, function(b, a) {
                c[b] && c[b].length > a && (c[b] = c[b].substr(0, a - 3) + "...")
            })
        },
        countSymbols: function(b) {
            return b.replace(/[\uD800-\uDBFF][\uDC00-\uDFFF]/g, "_").length
        },
        limitSymbols: function(b, a) {
            for (var g = "", h = 0, d, e, m = 0; m < b.length && (d = b.charAt(m), e = d.charCodeAt(0), g += d, 55296 <= e && 56319 >= e && (g += b.charAt(++m)), ++h != a); m++);
            return g
        },
        setCookie: function(b, a, g,
                            d) {
            b = b + "=" + (a ? encodeURIComponent(a) : "");
            g && (g = 0 < g ? (new Date((new Date).getTime() + g)).toGMTString() : "Thu, 01 Jan 1970 00:00:01 GMT", b += ";expires=" + g);
            d && (b += ";path=" + d);
            e.cookie = b
        }
    }
}(jQuery, window, document);
Events = function(b) {
    function d() {
        c("trigger", arguments)
    }
    function e() {
        c("on", arguments)
    }
    function a() {
        c("off", arguments)
    }
    function c(a, c) {
        var d = b(c[0]),
            g = f(c[1]),
            e = b.makeArray(c).slice(2),
            g = [g.join(" ")].concat(e);
        d[a].apply(d, g)
    }
    function f(a) {
        return b.map(a.split(" "), function(b) {
            b = b.split(".", 2);
            var a = g[b[0]];
            a && (b[0] = a);
            return b.join(".")
        })
    }
    var g = {};
    return {
        on: e,
        off: a,
        fire: d,
        refire: function(b, c, f) {
            a(b, c);
            e(b, c, f);
            d(b, c)
        },
        alias: function(b, a) {
            g[b] = a
        }
    }
}(jQuery);
Browser = function(b, d, e, a) {
    function c(b) {
        return -1 !== g.indexOf(b)
    }
    function f() {
        b(e.body).addClass("foo").removeClass("foo")
    }
    var g = d.navigator && d.navigator.userAgent || "";
    if (c("IEmobile/11")) a.on(d, "turbolinks:change", f);
    return {
        isUserAgent: c,
        notUserAgent: function(b) {
            return -1 === g.indexOf(b)
        }
    }
}(jQuery, window, document, Events);
Config = function(b, d, e) {
    function a(b) {
        f ? k.push(b) : b.apply(this)
    }
    function c(b, a) {
        if (null === b) throw Error("JS configuration not loaded yet (" + a + ")");
        if ("undefined" === typeof b[a]) throw Error("JS configuration not found for key " + a);
        return b[a]
    }
    var f = !0,
        g = null,
        h = null,
        k = [];
    e.on(d, "turbolinks:fetch", function() {
        f = !0
    });
    e.on(d, "custom:ready", function() {
        h = $(d.body).data("config-cache")
    });
    return {
        init: function(b) {
            g = b;
            f = !1;
            $(d.body).data("config-cache", h = g);
            for (b = 0; b < k.length; ++b) k[b].apply(this);
            k = []
        },
        once: a,
        ajax: function(b) {
            e.on(d, "turbolinks:fetch", function() {
                a(b)
            })
        },
        always: function(b) {
            e.on(d, "custom:ready", function() {
                a(b)
            })
        },
        latest: function(b) {
            return c(g, b)
        },
        cached: function(b) {
            return c(h, b)
        }
    }
}(window, document, Events);
Turbolinkz = function(b, d, e, a, c, f, g, h) {
    function k() {
        return g == h || !g.supported || !! (m("IEMobile/10.") || m("Android") && m("Firefox") || (m("Android 2.") || m("Android 4.0")) && m("Mobile Safari") && n("Chrome") && n("Windows Phone"))
    }
    function l(b) {
        var a;
        (a = 0 === b.indexOf("/") && "http:" === d.location.protocol && p ? "https://" + d.location.host + b : !1) ? d.location.href = a : k() ? d.location.href = b : g.visit(b)
    }
    var m = c.isUserAgent,
        n = c.notUserAgent,
        p = !1;
    k() ? a.alias("custom:ready", "ready") : (a.alias("custom:ready", "page:change"), a.alias("turbolinks:before-unload", "page:before-unload"), a.alias("turbolinks:fetch", "page:fetch"), a.alias("turbolinks:change", "page:change"));
    a.on(e, "ready", function() {
        k() && b(e.body).attr("data-no-turbolink", !0)
    });
    f.always(function() {
        p = f.latest("redirect_from_http_to_https")
    });
    return {
        visit: l,
        reload: function() {
            l(d.location.toString())
        }
    }
}(jQuery, window, document, Events, Browser, Config, window.Turbolinks);
Actions = function(b, d, e, a) {
    function c(a) {
        return function(c) {
            var d = f[b(this).data(a)];
            d && (d.handler(this, c), !1 !== d.options.preventDefault && c.preventDefault())
        }
    }
    var f = {};
    b(d).on("submit", "form[data-action]", c("action")).on("click", "a[data-action]", c("action")).on("click", "input[data-action]", c("action")).on("click", "button[data-action]", c("action")).on("change", "select[data-action]", c("action")).on("input paste", "input[data-input]", a.debounce(function(a) {
        return e.notUserAgent("Trident/") ? a : function() {
                var c = b(this),
                    f = c.data("oldVal") || "",
                    d = c.val();
                if (f !== d) return c.data("oldVal", d), a.apply(this, arguments)
            }
    }(c("input")), 500));
    return function(b, a, c) {
        f[b] = {
            handler: a,
            options: c || {}
        }
    }
}(jQuery, document, Browser, Utils);
Ajax = function(b, d) {
    function e(a, c, e) {
        var k = d.$tag$(c);
        a = b.extend(!0, {
            url: k.data("url") || k.attr("href"),
            type: a,
            data: d.serialize(k),
            context: c
        }, e);
        return b.ajax(a)
    }
    function a(a, c) {
        c = b.extend(!0, {}, c, {
            headers: {
                "X-CSRF-Token": d.getCsrfToken()
            }
        });
        return e("POST", a, c)
    }
    function c(c, d) {
        d = b.extend(!0, {}, d, {
            data: {
                _method: "PUT"
            }
        });
        return a(c, d)
    }
    return {
        get: function(b, a) {
            return e("GET", b, a)
        },
        post: a,
        put: c,
        _put: c,
        _delete: function(c, d) {
            d = b.extend(!0, {}, d, {
                data: {
                    _method: "DELETE"
                }
            });
            return a(c, d)
        }
    }
}(jQuery, Utils);
Mutex = function(b, d) {
    function e(c, f) {
        var e = b(c);
        if (e.data("mutex")) return !1;
        e.data("mutex", !0);
        f && (e.addClass("item-disabled"), d.setTimeout(function() {
            a(c);
            e.removeClass("item-disabled")
        }, f));
        return !0
    }
    function a(a) {
        b(a).removeData("mutex")
    }
    return {
        acquire: e,
        vacquire: function(c, d) {
            var g = b(c),
                h = e(g, d);
            if (!c.form) return h;
            if (!h) return !1;
            h = g.val();
            if (g.data("mutex-val") === h) return a(g), !1;
            g.data("mutex-val", h);
            return !0
        },
        release: a
    }
}(jQuery, window);
Pages = function(b, d, e) {
    function a(b, a, d) {
        b = b + "/" + a;
        c[b] ? c[b].push(d) : c[b] = [d]
    }
    var c = {};
    e.on(d, "custom:ready", function(a) {
        var e = b(d.body).data("id"),
            h = e && e.split("/") || [],
            e = h[0],
            h = h[1],
            h = b.unique((c["*/*"] || []).concat(c[e + "/*"] || []).concat(c[e + "/" + h] || [])),
            k;
        for (k in h) h[k].call(this, a)
    });
    return function(b, c) {
        for (var d in b) {
            var e = b[d];
            if ("*" === d) "*" === e && a(d, e, c);
            else if ("*" === e) a(d, e, c);
            else for (var l in e) a(d, e[l], c)
        }
    }
}(jQuery, document, Events);
window.AfmGPT = window.AfmGPT || function(b, d, e, a) {
        function c(b, a, c) {
            d.googletag.cmd.push(function() {
                var e = d.googletag.defineSlot(b, a, c).addService(d.googletag.pubads());
                d.googletag.enableServices();
                d.googletag.display(c);
                h[c] = e
            })
        }
        function f(b) {
            d.googletag.cmd.push(function() {
                d.googletag.pubads().refresh([b])
            })
        }
        var g = !1,
            h = {};
        a.on(e, "custom:ready", function() {
            g = d.googletag && d.googletag.apiReady
        });
        return {
            render: function(b, a, m) {
                if (!g) {
                    g = !0;
                    d.googletag = d.googletag || {};
                    d.googletag.cmd = d.googletag.cmd || [];
                    var n = e.createElement("script");
                    n.async = !0;
                    n.type = "text/javascript";
                    n.src = ("https:" == e.location.protocol ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";
                    var p = e.getElementsByTagName("script")[0];
                    p.parentNode.insertBefore(n, p)
                }(n = h[m]) ? f(n) : c(b, a, m)
            }
        }
    }(jQuery, window, document, Events);
PageLoader = function(b, d, e) {
    function a() {
        b("#loaderMother").show()
    }
    function c() {
        b("#loaderMother").hide()
    }
    e.on(d, "custom:ready", c);
    e.on(d, "turbolinks:fetch", a);
    return {
        show: a,
        hide: c
    }
}(jQuery, document, Events);
AjaxUI = function(b, d) {
    return {
        get: function(e, a) {
            d.show();
            return b.get(e, a).always(PageLoader.hide)
        }
    }
}(Ajax, PageLoader);
AskfmAd = function(b, d, e, a, c) {
    function f(a) {
        if (!a) return g.call(this);
        b(this).html(a).addClass("loader").show()
    }
    function g() {
        var a = b(this),
            c = a.find(".askfm-default-ad").show();
        a.toggle(0 < c.length)
    }
    function h(a, c) {
        if ("error" === c) {
            var d = b(this).data("count-url");
            d && b.get(d)
        }
        g.call(this)
    }
    function k() {
        a.get(this).done(f).fail(h).always(function() {
            b(this).addClass("loaded")
        })
    }
    function l(a) {
        var c = ".askfm-ad-slot[data-url]";
        a || (c += ":not(.loaded)");
        return function() {
            b(c).parent(":visible").children(c).each(k)
        }
    }
    e.on(d, "custom:ready", l(!0));
    e.on(d, "custom:resize", l(!1))
}(jQuery, document, Events, Ajax, Utils);
Atcomplete = function(b, d, e, a, c) {
    function f() {
        return s = s || new a(/(?:^|\s)(@(?:[a-z][a-z0-9_]*)?)/i, /([a-z0-9_]*)/, 50)
    }
    function g(a, d) {
        var e = b(a),
            f = e.data("friends");
        switch (f) {
            case c:
                e.data("friends", !1), f = e.data("atcomplete"), b.getJSON(f, function(b) {
                    e.data("friends", b);
                    q.call(a)
                });
            case !1:
                return !1;
            default:
                return h(f, d)
        }
    }
    function h(b, a) {
        for (var c = [], d = 0, e = b.length; d < e && (0 !== b[d].login.lastIndexOf(a, 0) || (c.push(b[d]), 7 !== c.length)); d++);
        return c
    }
    function k(a) {
        return function(c) {
            var d = "<b>@" + c.login.substring(0,
                    a) + "</b>" + c.login.substring(a);
            return b('<div class="item"> <div class="avatar"></div> <div class="name"></div> <div class="login" dir="ltr"></div></div>').data("login", c.login).find(".avatar").css("background-image", "url(" + c.avatar + ")").end().find(".name").text(c.name).end().find(".login").html(d).end()
        }
    }
    function l(a, c) {
        e.off(a, "keydown.atcomplete");
        (c || b(a).next(".atcomplete")).hide()
    }
    function m(a) {
        var c = b(a);
        return function() {
            c.data("noblur", !0)
        }
    }
    function n(a, c) {
        return function() {
            var d = b(a).removeData("noblur"),
                g = b(this).data("login");
            e.fire(d, "focus");
            f().replace(a, "@" + g + " ");
            e.fire(d, "input");
            l(d, c)
        }
    }
    function p(b) {
        return function(a) {
            var c = a.keyCode;
            if (9 === c || 13 === c || 27 === c || 38 === c || 40 === c) switch (a.preventDefault(), c) {
                case 13:
                    e.fire(b.find(".active"), "click");
                    break;
                case 27:
                    l(this, b);
                    break;
                case 9:
                    a.shiftKey && (c = 38);
                case 38:
                case 40:
                    a = b.find(".active").removeClass("active"), c = a[38 === c ? "prev" : "next"](), (c.length ? c : a).addClass("active")
            }
        }
    }
    function q() {
        var a = f().token(this).value,
            c = [],
            d = "";
        0 < a.length && (d = a.substring(1),
            c = g(this, d));
        c && 0 === c.length ? l(this) : (a = c, d = d.length, c = b(this).next(".atcomplete"), 0 === c.length && (c = b('<div class="atcomplete"></div>').insertAfter(this), e.on(c, "mousedown", ".item", m(this)), e.on(c, "click", ".item", n(this, c))), a = b.map(a, k(d)), e.off(this, "keydown.atcomplete"), e.on(this, "keydown.atcomplete", p(c)), a.length && a[0].addClass("active"), c.empty().append(a).show())
    }
    function v() {
        var a = b(this);
        a.data("noblur") || l(a)
    }
    function u(a) {
        a = b("input[data-atcomplete],textarea[data-atcomplete]", a.target);
        e.off(a, ".atcomplete");
        e.on(a, "input.atcomplete click.atcomplete", q);
        e.on(a, "blur.atcomplete", v)
    }
    var s;
    if (function() {
            var b = d.createElement("textarea");
            return "selectionStart" in b && "setSelectionRange" in b
        }()) e.on(d, "custom:ready lightbox:change", u)
}(jQuery, document, Events, Cursores);
Form = function(b, d, e) {
    function a(a, e) {
        var h = d.$tag$(a),
            k = b("<form>").attr({
                action: h.data("url") || h.attr("href"),
                target: c("IEmobile/11") || c("iPhone") && (c("FBAN") || c("Twitter") || c("Instagram")) ? null : h.data("window"),
                method: "POST"
            }),
            h = b.extend({}, e, d.serialize(h), {
                authenticity_token: d.getCsrfToken()
            }),
            l;
        for (l in h) b("<input>").attr({
            name: l,
            value: h[l]
        }).appendTo(k);
        k.hide().appendTo("body").submit()
    }
    var c = e.isUserAgent;
    return {
        post: function(b) {
            a(b)
        },
        put: function(b) {
            a(b, {
                _method: "PUT"
            })
        },
        "delete": function(b) {
            a(b, {
                _method: "DELETE"
            })
        }
    }
}(jQuery, Utils, Browser);
Button = function(b, d, e, a) {
    function c(a) {
        a = b(a);
        b(a.data("target")).submit()
    }
    return {
        submit: c,
        submitOnce: function(b) {
            e.acquire(b) && c.call(this, b)
        },
        postOnce: function(c) {
            if (e.acquire(c, 2E3)) {
                var d = b(c).data("method") || "post";
                if (a[d]) a[d](c)
            }
        },
        getOnce: function(a) {
            e.acquire(a) && d.visit(b(a).data("url"))
        }
    }
}(jQuery, Turbolinkz, Mutex, Form);
Captcha = function(b, d, e, a, c) {
    function f(b, c, d) {
        d || b.is(":visible") ? a.get(b[0]).done(function(a) {
                b.html(a || "").toggle( !! a);
                g(b);
                c && c()
            }) : c && c()
    }
    function g(b) {
        d.clearTimeout(h);
        0 !== b.find(".captcha.askfm").length && (h = d.setTimeout(function() {
            f(b)
        }, 3E5))
    }
    var h = null;
    return {
        reload: function(a) {
            a = b(".captchaContainer", a);
            1 === a.length && f(a, null, !0)
        },
        reloadAction: function(a) {
            if (e.vacquire(a)) {
                var d = function() {
                        e.release(a)
                    }, g = b(a),
                    h = !! a.form,
                    p = h ? b(".captchaContainer", a.form) : g.closest(".captchaContainer");
                if (1 !== p.length) return d();
                h && p.data("params", c.serialize(g));
                f(p, d)
            }
        },
        onRecaptcha2Load: function() {
            b(".g-recaptcha").each(function() {
                d.grecaptcha.render(this, {
                    size: b(this).data("size"),
                    sitekey: b(this).data("sitekey"),
                    "expired-callback": d.grecaptcha.reset
                })
            })
        },
        toggleUrl: function(a, c) {
            var d = b(".captchaContainer", a);
            d.data("url", d.data(c + "-url"))
        }
    }
}(jQuery, window, Mutex, Ajax, Utils);
Recaptcha2onLoad = Captcha.onRecaptcha2Load;
Checkbox = function(b, d) {
    function e(b, a) {
        return function(e, h) {
            h = b !== d ? b : !(a ? !0 === h : "true" === h);
            return a ? h : h ? "true" : "false"
        }
    }
    function a(a, d) {
        var g = b(a),
            h = g.data("target"),
            h = h ? b("#" + h) : g,
            k = h.next("input");
        h.toggleClass(g.data("class"), d);
        k.is(":checkbox") ? k.prop("checked", e(d, !0)) : k.val(e(d, !1))
    }
    return {
        toggle: a,
        onToggle: function(b) {
            a(b)
        }
    }
}(jQuery);
Flash = function(b, d, e, a) {
    function c() {
        m.stop(!0, !0).hide()
    }
    function f() {
        return b(this).delay(a.latest("flash:delay")).fadeOut("slow").dequeue()
    }
    function g(b, a) {
        var c = -1 === b.indexOf("<a");
        n.html(b);
        m.finish().removeClass("alert notice").addClass(a).show();
        c && (n.append(p), m.queue(f))
    }
    function h(b) {
        g(b, "alert")
    }
    function k(b) {
        g(b, "notice")
    }
    function l(b, a) {
        b ? h(b) : a ? k(a) : c()
    }
    var m = null,
        n = null,
        p = null;
    e.on(d, "custom:ready lightbox:change lightbox:close", function(a) {
        m = b(".flash-message", "lightbox:close" === a.type ? d : a.target).first();
        n = m.find("p");
        p = m.find(".icon-close");
        m.filter(":visible").queue(f)
    });
    return {
        error: h,
        notice: k,
        message: l,
        hideAction: c,
        showAction: function(a) {
            var c = b(a);
            a = c.data("error");
            c = c.data("notice");
            l(a, c)
        }
    }
}(jQuery, document, Events, Config);
Lightbox = function(b, d, e, a, c, f, g, h, k) {
    function l(a) {
        if (w) {
            var c = b(d);
            a = c.scrollTop();
            c = c.height();
            0 < a && b(e.body).height() === c && (a = 0);
            t.css({
                top: a,
                height: c
            })
        }
    }
    function m() {
        a.fire(r, "lightbox:close");
        a.off(e, ".lightbox");
        r.remove();
        t = r = null;
        b(e.documentElement).removeClass("lightbox-open")
    }
    function n() {
        a.on(r, "focusin.lightbox", "input", function() {
            w = !1
        });
        a.on(r, "focusout.lightbox", "input", function() {
            w = !0
        });
        a.on(e, "scroll.lightbox", k.debounce(l, 100));
        a.on(e, "custom:resize.lightbox", l);
        l()
    }
    function p() {
        t.on("click",

            function() {});
        a.on(e, "turbolinks:change.lightbox", m);
        a.on(e, "keyup.lightbox", function(b) {
            27 == b.keyCode && m()
        });
        a.on(e, "click.lightbox", ".viewport", function(b) {
            b.target === this && (m(), b.preventDefault())
        })
    }
    function q(c, d) {
        d = b.extend({
            event: !0,
            modal: !1,
            css_class: ""
        }, d);
        var f;
        f = d;
        r || (r = b('<div id="lightbox" class="util-overlay">  <div class="viewport util-vcontainer">    <div class="util-vcenter"></div></div></div>'), t = r.find(".viewport"), n(), f.modal || p(), b(e.documentElement).addClass("lightbox-open"),
            b(e.body).append(r));
        f = r;
        f.find(".util-vcenter").addClass(d.css_class).html(c);
        d.event && a.fire(r, "lightbox:change")
    }
    function v(a, c) {
        for (var d in a) if (a.hasOwnProperty(d)) break;
        a.hasOwnProperty(c) && (d = c);
        var e = '<a href="#" class="util-close" data-action="PopupClose"></a><img class="gallery-image" src="' + a[d] + '" /><nav><a href="#" class="icon-caret-left" data-action="ImagesPrev"></a>' + b.map(a, function(b, a) {
                return '<a href="#"' + (a === d ? ' class="active"' : "") + ' style="background-image:url(' + a + ')" data-action="ImagesSelect" data-url="' + b + '"></a>'
            }).join("") + '<a href="#" class="icon-caret-right" data-action="ImagesNext"></a></nav>';
        q(e, {
            event: !1
        })
    }
    function u(b) {
        switch (typeof b) {
            case "string":
                q(b.replace('autofocus="autofocus"', ""));
                break;
            case "object":
                h.message(b.error, b.notice);
            default:
                m()
        }
        f.release(this);
        g.hide()
    }
    function s(c) {
        return function(d) {
            d = b("#lightbox nav .active")[c]('[data-action="ImagesSelect"]');
            a.fire(d, "click")
        }
    }
    var r = null,
        t = null,
        w = !0;
    a.on(e, "custom:ready", function() {
        var a = b("#preloaded-lightbox");
        1 === a.length && q(a.html(), a.data("options"))
    });
    return {
        popupAction: function(a) {
            if (f.acquire(a)) {
                var d = b(a);
                g.show();
                d.is("form") ? b.ajax(a.action, {
                        data: d.serializeArray(),
                        type: a.method,
                        context: a
                    }).done(u) : c[d.data("method") || "get"](a).done(u)
            }
        },
        imageAction: function(a) {
            if (f.acquire(a)) {
                var c = b(a),
                    d = c.data("url"),
                    c = c.find("img").attr("onerror");
                d && q('<a href="#" class="util-close" data-action="PopupClose"></a><img src="' + d + '"' + (c ? ' onerror="' + c + '"' : "") + "/>", {
                    event: !1
                });
                f.release(a)
            }
        },
        imagesAction: function(a) {
            if (f.acquire(a)) {
                var c = b(a),
                    d = c.data("urls"),
                    c = c.data("open-url");
                v(d, c);
                f.release(a)
            }
        },
        selectImageAction: function(a) {
            a = b(a);
            var c = b("#lightbox nav .active"),
                d = b("#lightbox img"),
                e = a.data("url"),
                f = c.prop("class");
            d.prop("src", "").prop("src", e);
            c.removeClass(f);
            a.addClass(f)
        },
        prevImageAction: s("prev"),
        nextImageAction: s("next"),
        closeAction: function(a) {
            r && b.contains(r[0], a) && m()
        }
    }
}(jQuery, window, document, Events, Ajax, Mutex, PageLoader, Flash, Utils);
FormXHR = function(b, d, e, a, c, f, g, h, k, l) {
    function m(a, c) {
        var d = {};
        b.each(c, function(c, e) {
            var f = e.match(/[a-z_]+_at/),
                f = f ? f[0] : e;
            (e = a[e]) && (d[f] = (d[f] || b()).add(e))
        });
        b.each(d, function(b, a) {
            var c = a.is("select") ? a : a.parent();
            c.addClass("invalid-input");
            e.off(a, ".formxhr");
            e.on(a, "focus.formxhr blur.formxhr", function() {
                c.removeClass("invalid-input");
                e.off(a, ".formxhr")
            })
        })
    }
    function n(a) {
        g.error(a.error);
        a.fields && m(this, a.fields);
        b(this.authenticity_token).val(h.getCsrfToken());
        k.reload(this);
        f.hide();
        c.release(this)
    }
    function p(b) {
        b ? b.error ? n.call(this, b) : b.url ? a.visit(b.url) : (f.hide(), c.release(this), l.closeAction(this), b.notice && g.notice(b.notice)) : n.call(this, {
                error: "No data received"
            })
    }
    return {
        submit: function(a) {
            c.acquire(a) && (f.show(), b.json(a.action, {
                data: b(a).serializeArray(),
                type: a.method,
                context: a
            }, p))
        }
    }
}(jQuery, document, Events, Turbolinkz, Mutex, PageLoader, Flash, Utils, Captcha, Lightbox);
FormXHR2 = function(b) {
    return {
        upload: function(d, e) {
            var a = new FormData(d),
                c;
            for (c in e) a.append(c, e[c]);
            return b.ajax({
                url: b(d).data("upload-url"),
                type: "POST",
                data: a,
                async: !0,
                cache: !1,
                contentType: !1,
                processData: !1,
                context: d
            })
        }
    }
}(jQuery);
GoogleAnalytics = function(b, d, e, a) {
    function c() {
        f({
            event: "pageview-ajax"
        })
    }
    function f(c) {
        !g && a.latest("gtm") && (d.dataLayer = d.dataLayer || [], d.dataLayer.push(b.extend({
            "page.category": a.cached("page:category"),
            "page.type": a.cached("page:type"),
            "user.id": a.latest("page:user_id")
        }, c)))
    }
    var g = !0;
    a.once(function() {
        if (a.latest("gtm")) {
            f();
            g = !1;
            d.dataLayer = d.dataLayer || [];
            d.dataLayer.push({
                "gtm.start": (new Date).getTime(),
                event: "gtm.js"
            });
            var b = e.getElementsByTagName("script")[0],
                k = e.createElement("script");
            k.async = !0;
            k.src = "//www.googletagmanager.com/gtm.js?id=GTM-W5DVX4";
            b.parentNode.insertBefore(k, b);
            a.ajax(c)
        }
    });
    return {
        push: f
    }
}(jQuery, window, document, Config);
GTM = function(b, d, e, a) {
    function c(b, a, c) {
        $.each($.trim(a).split(/\s+/), function() {
            b[this] = c
        })
    }
    function f(b) {
        var a = $(this),
            c = a.data("gtm"),
            d = $.isPlainObject(c);
        b = d ? c.name : c;
        a = [a, b].concat(d ? c.meta : []);
        (h[b] || k).apply(l, a)
    }
    var g = {}, h = {}, k = function(b, a) {
        this.push({
            event: a
        })
    }, l = {
        push: function(b) {
            a && a.push(b)
        },
        opts: {}
    };
    e.always(function() {
        l.opts = {
            pageCategory: e.cached("page:category")
        }
    });
    $(d).on("submit", "form[data-gtm]", f).on("click", "a[data-gtm]", f);
    return {
        routine: function(b, a) {
            c(g, b, a)
        },
        element: function(b,
                          a) {
            c(h, b, a)
        },
        exec: function() {
            var b = $.makeArray(arguments);
            g[b[0]].apply(l, b)
        }
    }
}(jQuery, document, Config, window.GoogleAnalytics);
HoverToActive = function(b, d, e, a) {
    function c() {
        for (var b = 0; b < d.styleSheets.length; b++) {
            var a = d.styleSheets[b];
            if (a.cssRules) for (var c = 0; c < a.cssRules.length; c++) {
                var e = a.cssRules[c];
                e.selectorText && (e.selectorText = e.selectorText.replace(":hover", ":active"))
            }
        }
    }
    b = a.isUserAgent;
    a = a.notUserAgent;
    if ((b("iPhone") || b("iPad") || b("iPod")) && a("CriOS")) e.on(d, "ready", c)
}(jQuery, document, Events, Browser);
ScrollViewMore = function(b, d, e, a, c) {
    function f(a) {
        return b(a).parents("#MassAskSearch").filter(function() {
            return "visible" !== b(this).css("overflowY")
        }).add(e).last()
    }
    var g = c.debounce(function(c) {
        var f = b(".viewMore", c.target);
        if (1 !== f.length) a.off(c.target, "scroll.viewMore");
        else {
            var g = b(c.target === e ? d : c.target);
            c = g.height();
            g = g.scrollTop() + c;
            c *= .65;
            f.position().top - g < c && f.click()
        }
    }, 100);
    a.on(e, "custom:ready custom:resize lightbox:change pager:change", function(c) {
        b(".viewMore", c.target).each(function() {
            a.refire(f(this), "scroll.viewMore", g)
        })
    });
    return {
        selector: ".viewMore"
    }
}(jQuery, window, document, Events, Utils);
Pager = function(b, d, e, a) {
    function c(c) {
        c || (c = b(".item-pager").first());
        return c.find(".item").length || c.find(a.selector).length ? !1 : !0
    }
    e.on(d, "custom:ready pager:change", function(a) {
        a = a.target === d ? b(".item-pager").first() : b(a.target);
        var e;
        e = a.siblings(".emptyViewContent,.emptyResults");
        e = e.length ? e : a.parent().siblings(".emptyViewContent,.emptyResults");
        e.length && e.toggle(c(a))
    });
    return {
        pagerSelector: ".item-pager",
        isEmpty: c,
        $prependWithItems: function(a, c) {
            var d = b(a),
                k = b(c).prependTo(d);
            e.fire(d, "pager:change");
            return k
        },
        $replaceWithItems: function(a, c) {
            var d = b(c).replaceAll(a),
                k = d.closest(".item-pager"),
                d = d.find(".item").length;
            e.fire(k, "pager:change");
            return d
        },
        $removeClosestItem: function(a) {
            a = b(a).closest(".item");
            var c = a.closest(".item-pager");
            a.remove();
            e.fire(c, "pager:change")
        },
        $substituteItems: function(a, c) {
            var d = b(a).html(c),
                d = d.is(".item-pager") ? d : d.find(".item-pager");
            e.fire(d, "pager:change")
        }
    }
}(jQuery, document, Events, ScrollViewMore);
MassAsk = function(b, d, e, a, c, f, g, h, k) {
    function l(b) {
        var a;
        b = b.data("logins");
        var c = 0;
        for (a in b) b.hasOwnProperty(a) && c++;
        return c
    }
    function m(b, a, c) {
        a = b.find("#MassAskSearch").data(a);
        g.error(a);
        c && b.find(".selectFriendsCounter").addClass("error")
    }
    function n(b, a) {
        b.toggleClass("listItemCheckbox-checked", a);
        b.find(".listItemCheckbox").toggleClass("listItemCheckbox-checked", a)
    }
    function p(b, a, c) {
        b = b.find('#MassAskSearch a[data-login="' + a + '"]');
        b.length && n(b, c)
    }
    function q(b, a) {
        var c = b.data("logins") || {},
            c = a(c);
        b.data("logins", c);
        var c = l(b),
            d = f.latest("mass_ask_limit");
        b.find(".selectFriendsCounter").removeClass("error").text(c + "/" + d)
    }
    function v(a) {
        var c = a.data("logins") || {};
        b.each(c, function(b) {
            p(a, b, !0)
        })
    }
    function u(a) {
        q(a, function(c) {
            b.each(c, function(b, d) {
                if (d) return !0;
                p(a, b, !1);
                delete c[b]
            });
            return c
        })
    }
    function s(b) {
        var a = b[0].elements["question[anonymous]"] ? b[0].elements["question[anonymous]"][1] : null;
        a && a.checked ? (u(b), b.find('a[data-allow-anonymous-questions="false"]').addClass("item-disabled")) : b.find(".item-disabled").removeClass("item-disabled")
    }
    function r(a) {
        b(a[0].elements["question[logins][]"]).remove();
        b.each(a.data("logins") || {}, function(c) {
            b("<input/>", {
                type: "hidden",
                name: "question[logins][]",
                value: c
            }).appendTo(a)
        })
    }
    function t(a) {
        a = b(a.target).closest("form");
        v(a);
        s(a)
    }
    e.on(d, "custom:ready lightbox:change", function(a) {
        a = b("#MassAskSearch " + h.pagerSelector);
        e.refire(a, "pager:change", t)
    });
    return {
        onSearch: function(d) {
            a.acquire(d) && c.get(d).done(function(c) {
                var e = b(d.form).find(h.pagerSelector);
                h.$substituteItems(e, c || null);
                a.release(d)
            })
        },
        onToggleAnonymous: function(a) {
            a = b(a).closest("form");
            s(a)
        },
        onToggleUser: function(a) {
            a = b(a);
            var c = a.closest("form"),
                d = !a.hasClass("listItemCheckbox-checked"),
                e = a.data("login"),
                g = a.data("allow-anonymous-questions");
            a.hasClass("item-disabled") ? m(c, "disabled-item-error") : d && l(c) === f.latest("mass_ask_limit") ? m(c, "max-users-error", !0) : (q(c, function(b) {
                        d ? b[e] = g : delete b[e];
                        return b
                    }), n(a, d))
        },
        onSubmit: function(a) {
            var c = b(a),
                d = l(c);
            0 === d ? m(c, "min-users-error", !0) : (r(c), k.toggleUrl(c, 1 === d ? "single" : "mass"), FormXHR.submit(a))
        },
        loginCount: l
    }
}(jQuery, document, Events, Mutex, AjaxUI, Config, Flash, Pager, Captcha);
Media = function(b, d, e, a, c) {
    function f(b, a, d, f) {
        var h = b.find(".visualItemPlayIcon, .visualItemWatermarkIcon-youTube"),
            k = a.data("src"),
            l = a.attr("src"),
            n = "IMG" === a.prop("tagName"),
            x = new Date;
        h.toggle(d).addClass("loader");
        e.off(a, "load.media");
        e.on(a, "load.media", function() {
            h.toggle(!d).removeClass("loader");
            n && b.find(".visualItemWatermarkIcon-animatedGif").toggle(d);
            n && d && c.exec("gif-load", new Date - x)
        });
        a.data("src", l).attr("src", k);
        b.toggleClass("playing", d);
        f || b.toggleClass("stopped", !d);
        g();
        n && c.exec("gif-toggle",
            d, m, f)
    }
    function g() {
        l || (e.on(d, "turbolinks:before-unload", function(a) {
            a = b(".playing", a.originalEvent.data);
            e.fire(a, "click")
        }), l = !0)
    }
    function h(a) {
        return function() {
            var c = b(this),
                d = c.hasClass("playing"),
                e = c.hasClass("stopped"),
                g = this.getBoundingClientRect();
            (0 < g.bottom && g.top < a ? d || e : !d) || (e = c.find("img"), f(c, e, !d, !0))
        }
    }
    function k() {
        b(".side-column").is(":visible") ? (m = !0, e.refire(d, "scroll.gif", n)) : (m = !1, e.off(d, "scroll.gif"))
    }
    var l = !1,
        m = !1,
        n = Utils.debounce(function() {
                var a = b(window).height();
                b('a[data-action="GifToggle"]').each(h(a))
            },
            100);
    a.once(function() {
        e.on(d, "custom:ready custom:resize", k)
    });
    return {
        gifToggleAction: function(a) {
            a = b(a);
            var c = a.find("img"),
                d = a.hasClass("playing");
            f(a, c, !d)
        },
        iframeToggleAction: function(a) {
            a = b(a);
            var c = a.find("img"),
                d = a.find("iframe"),
                e = a.hasClass("playing");
            if (!e) {
                var g = "uniq" + Math.random();
                d.remove().attr("name", g).appendTo(a)
            }
            f(a, d, !e);
            c.toggle(e);
            d.toggle(!e)
        }
    }
}(jQuery, document, Events, Config, GTM);
Notifications = function(b, d, e, a, c, f, g, h) {
    function k() {
        d.clearTimeout(p);
        var a = g.latest("poller:interval");
        a && (p = d.setTimeout(l, a))
    }
    function l() {
        var a = q.data("poll-url");
        a && b.getJSON(a).done(m)
    }
    function m(a) {
        b(".notificationCounter").text(a).toggle( !! a);
        k()
    }
    function n(a) {
        a.target.href || (a = b(this).data("url")) && f.visit(a)
    }
    var p = null,
        q = null;
    g.always(function() {
        q = b("#notificationPollPath");
        0 < q.length && k()
    });
    return {
        initClickActions: function() {
            a.on(e, "click.notifications", ".streamItem-notification", n);
            a.on(e, "turbolinks:before-unload", function() {
                a.off(e, ".notifications")
            })
        },
        deleteAction: function(a) {
            c._delete(a);
            h.$removeClosestItem(a)
        }
    }
}(jQuery, window, document, Events, Ajax, Turbolinkz, Config, Pager);
OnlineStatus = function(b, d, e) {
    function a(a, b) {
        e.acquire(a) && d[b ? "post" : "_delete"](a).done(c(b))
    }
    function c(a) {
        return function() {
            var c = b(this).parent().prev(),
                d = c.data("class");
            oldState = c.hasClass(d);
            if (oldState !== a) {
                var k = c.text(),
                    l = c.data("text");
                c.data("text", k).text(l);
                c.toggleClass(d)
            }
            e.release(this)
        }
    }
    return {
        showAction: function(b) {
            a(b, !0)
        },
        hideAction: function(b) {
            a(b, !1)
        }
    }
}(jQuery, Ajax, Mutex);
PhotoAnswer = function(b, d, e, a) {
    function c() {
        b(".loaderUpload", this).hide();
        this.reset();
        d.release(this)
    }
    function f(c) {
        var d = this.rid.value;
        b("#answerType").val("photo");
        b("#photoUrl").val(d);
        b("#photoThumb").bg(c.thumbUrl);
        b("#uploadForm").hide();
        b(".optionsBar-option.icon-upload-image,.optionsBar-option.icon-close,#photoThumb").toggle();
        a.notice(b(this).data("success-text"))
    }
    return {
        submitAction: function(a) {
            a.file.value && d.acquire(a) && (b(".loaderUpload", a).show(), e.upload(a, {
                ts: (new Date).getTime()
            }).done(f).fail(function(b) {
                Uploads.onFailure(b,
                    a)
            }).always(c))
        },
        removeAction: function(a) {
            b("#answerType").val(null);
            b("#photoUrl").val(null);
            b(".optionsBar-option.icon-upload-image,.optionsBar-option.icon-close,#photoThumb").toggle()
        }
    }
}(jQuery, Mutex, FormXHR2, Flash);
PhotoPoll = function(b, d, e, a) {
    function c() {
        b(".loaderUpload", this).hide();
        this.reset();
        d.release(this)
    }
    function f(c) {
        var d = b(this).data("index"),
            e = b("#photopollOptions").find(".choice-" + d);
        b("#photoUrl" + d).val(c.sequence_id);
        e.trigger("click").bg(c.thumbBigUrl).find(".empty").remove();
        a.notice(b(this).data("success-text"))
    }
    return {
        submitAction: function(a) {
            a.file.value && d.acquire(a) && (b(".loaderUpload", a).show(), e.upload(a, {
                ts: (new Date).getTime()
            }).done(f).fail(function(b) {
                Uploads.onFailure(b, a)
            }).always(c))
        }
    }
}(jQuery,
    Mutex, FormXHR2, Flash);
Poller = function(b, d, e, a) {
    function c() {
        d.clearTimeout(k);
        var a = e.latest("poller:interval");
        a && (k = d.setTimeout(f, a))
    }
    function f() {
        var a = b("#newItemsReady").data("poll-url");
        a && b.get(a, {
            counter: l
        }).done(g)
    }
    function g(b) {
        b && (l += a.$replaceWithItems("#newItemsReady", b));
        l < e.latest("poller:cache_limit") && c()
    }
    function h() {
        var a = 0;
        b(".item-poller-chunk .likeButton .counter").each(function() {
            a += parseInt(b(this).text())
        });
        return a
    }
    var k = null,
        l = 0;
    e.always(c);
    return {
        clickAction: function() {
            var d = b(".item-poller-chunk").children();
            c();
            b("#newItemsReady").hide();
            ProfileTabCounters.update({
                answers: l,
                likes: h
            });
            a.$prependWithItems(a.pagerSelector, d);
            d.addClass("highlight");
            l = 0
        }
    }
}(jQuery, window, Config, Pager);
PopupCallback = function(b, d) {
    function e(a, c) {
        var e = b(a),
            g = e.get(0);
        e.data("connected", c);
        d.toggle(g, c)
    }
    return {
        permit: {
            facebook: function(a) {
                e("#facebook_switch", a)
            },
            twitter: function(a) {
                e("#twitter_switch", a)
            },
            vkontakte: function(a) {
                e("#vkontakte_switch", a)
            }
        }
    }
}(jQuery, Checkbox);
PositionFixedFocus = function(b, d, e, a, c) {
    function f() {
        m && g()
    }
    function g() {
        m = !0;
        l.css("position", "absolute")
    }
    function h() {
        m = !1;
        l.css("position", "");
        d.setTimeout(function() {
            d.scrollTo(e.body.scrollLeft, e.body.scrollTop)
        }, 0)
    }
    function k() {
        l = b("#topMenu");
        a.on(e, "focusin", "input,textarea", g);
        a.on(e, "focusout", "input,textarea", h);
        a.on(e, "custom:resize", f)
    }
    var l = null,
        m = !1;
    c = c.isUserAgent;
    if (c("CriOS") || (c("iPad") || c("iPod")) && c("OS 7_")) a.on(e, "custom:ready", k)
}(jQuery, window, document, Events, Browser);
ProfileTabCounters = function(b, d, e) {
    var a = !1,
        c = {
            answers: ".profileTabAnswerCount",
            likes: ".profileTabLikeCount"
        };
    return {
        update: function(d) {
            a && b.each(d, function(a, d) {
                b.isFunction(d) && (d = d());
                b(c[a]).text(function(a, b) {
                    return parseInt(b) + parseInt(d)
                })
            })
        },
        enable: function() {
            a = !0;
            e.on(d, "turbolinks:before-unload", function() {
                a = !1
            })
        }
    }
}(jQuery, document, Events);
Pymk = function(b, d, e, a) {
    return {
        closeAction: function(c) {
            b(c).closest(".streamItem-pymk").remove();
            a.setCookie("skip_pymk", "1", null, "/")
        },
        clickAction: function(a) {
            if (e.acquire(a)) {
                var f = b(a).closest(".streamItem-pymk");
                f.addClass("loader");
                d.post(a).done(function(a) {
                    f.replaceWith(a)
                })
            }
        }
    }
}(jQuery, Ajax, Mutex, Utils);
Quantcast = function(b, d, e, a) {
    function c() {
        d._qevents.push(f({
            event: "click"
        }))
    }
    function f(c) {
        return b.extend({
            qacct: "p-r1DAFnUnQgqGA",
            uid: a.latest("page:user_id")
        }, c)
    }
    a.once(function() {
        d.qcdata = {};
        d._qevents = d._qevents || [];
        var b = e.createElement("script");
        b.src = ("https:" == e.location.protocol ? "https://secure" : "http://pixel") + ".quantserve.com/aquant.js?a=p-r1DAFnUnQgqGA";
        b.async = !0;
        b.type = "text/javascript";
        var h = e.getElementsByTagName("script")[0];
        h.parentNode.insertBefore(b, h);
        d.qcdata = f();
        a.ajax(c)
    })
}(jQuery,
    window, document, Config);
Questions = function(b, d, e, a, c) {
    function f(a) {
        var c = b("#question-count");
        if (c.length) return a = parseInt(c.text()) + a, c.text(a).toggle(0 < a), a
    }
    function g(c) {
        c = a.$prependWithItems(a.pagerSelector, c);
        f(1);
        b("#questions-delete").toggle(!0);
        c.addClass("highlight");
        e.release(this)
    }
    return {
        randomAction: function(a) {
            e.acquire(a) && d.post(a).done(g)
        },
        deleteAction: function(c) {
            d._delete(c);
            a.$removeClosestItem(c);
            c = f(-1);
            0 === c && b("#questions-delete").toggle(!1);
            1 === c && a.isEmpty() && b("#questions-delete").toggle(!1)
        },
        deleteAllAction: function(a) {
            if (e.acquire(a)) {
                var f = b(a).data("confirm");
                window.confirm(f) && (PageLoader.show(), d._delete(a).fail(PageLoader.hide).done(c.reload));
                e.release(a)
            }
        }
    }
}(jQuery, Ajax, Mutex, Pager, Turbolinkz);
(function(b, d, e, a) {
    function c() {
        var a = b(d).scrollTop() + m,
            c = b(d).height() - m,
            e = k.height() < h.height(),
            f = n + m;
        l.each(function() {
            var d = b(this),
                h = n + d.height(),
                k = d.data("$anchor"),
                l = g.position().top,
                m = k.position().top,
                k = k.height(),
                h = e && h < c && (0 < k ? m + k : l) < a;
            d.toggleClass("sticky-div", h).css({
                top: h ? f : 0
            })
        })
    }
    function f() {
        var a = b(this),
            c = a.data("stick-to");
        a.data({
            $anchor: b(c)
        }).css({
            width: a.width()
        })
    }
    var g, h, k, l, m, n;
    a.on(e, "custom:ready", function() {
        g = b(".main-content").presence();
        h = b(".main-column").presence();
        k = b(".side-column").presence();
        l = b("[data-stick-to]:visible").each(f).presence();
        m = b("#topMenu").height() || 0;
        n = g && parseInt(g.css("padding-top")) || 0
    });
    a.on(d, "resize scroll", function() {
        g && h && l && c()
    })
})(jQuery, window, document, Events);
Tagcomplete = function(b, d, e) {
    function a(a) {
        return function(c) {
            var d = c[0];
            c = c[1];
            return b('<div class="item"> <span class="count"></span> <div class="tag"></div></div>').data("tag", d).find(".tag").append([b("<mark/>").text(d.substring(0, a)), d.substring(a)]).end().find(".count").text(c).toggle(0 < c).end()
        }
    }
    function c(c, d, f) {
        d = b.map(d, a(f));
        f = c.data("$list");
        f || (f = b('<div class="tagcomplete"></div>'), e.on(f, "mousedown", ".item", h), e.on(f, "click", ".item", k(c)), c.data("$list", f).after(f));
        c = f;
        d[0].addClass("active");
        c.empty().append(d).show();
        return c
    }
    function f(a) {
        if (a = a.data("$list")) p && (p.abort(), p = null), a.hide()
    }
    function g(a, c) {
        c = c || a.attr("placeholder") || "";
        var d = a.data("$sizer");
        d || (d = b('<div style="position:absolute;visibility:hidden"/>'), a.data("$sizer", d).before(d));
        d = d.text(c).width();
        a.width(d + 20)
    }
    function h(a) {
        a.preventDefault()
    }
    function k(a) {
        return function() {
            var c = b(this).data("tag");
            f(a);
            a.val(c + " ");
            e.fire(a, "input")
        }
    }
    function l(a) {
        var c = b(this),
            d = c.data("$list"),
            g = a.keyCode;
        if (d && d.is(":visible")) switch (g) {
            case 13:
                e.fire(d.find(".active"), "click");
                break;
            case 27:
                f(c);
                break;
            case 9:
                a.shiftKey && (g = 38);
            case 38:
            case 40:
                c = 38 === g;
                g = d.find(".active").removeClass("active")[c ? "prev" : "next"]();
                g.length || (g = d.find(".item" + (c ? ":last" : ":first")));
                g.addClass("active");
                break;
            default:
                return
        } else switch (g) {
            case 13:
                if (d = c.val(), /\S/.test(d)) {
                    c.val(d + " ");
                    e.fire(c, "input");
                    break
                }
            default:
                return
        }
        a.preventDefault()
    }
    function m() {
        p && (p.abort(), p = null);
        var a = b(this),
            d = a.val(),
            e = /^#?\s*$/.test(d),
            h = / $/.test(d);
        g(a, d);
        e || h ? f(a) : (d = d.replace(/^#?\s*/, ""), e = [
                [d,
                    0]
            ], h = c(a, e, d.length), q(a, d, h, e))
    }
    function n() {
        var a = b(this);
        f(a)
    }
    var p, q = Utils.debounce(function(a, d, e, f) {
        var g = a.data("tagcomplete"),
            h = {
                hashtag: d
            };
        e.addClass("loader");
        p = b.getJSON(g, h, function(b) {
            e.removeClass("loader");
            b && b[0] && b[0][0] && (f = f[0][0] === b[0][0] ? b : f.concat(b), c(a, f, d.length))
        })
    }, 100);
    e.on(d, "custom:ready lightbox:change", function(a) {
        a = b("input[data-tagcomplete]", a.target);
        a.length && (g(a), e.off(a, ".tagcomplete"), e.on(a, "keydown.tagcomplete", l), e.on(a, "input.tagcomplete", m), e.on(a, "blur.tagcomplete",
            n))
    })
}(jQuery, document, Events);
(function(b, d, e, a) {
    function c() {
        var c = b(this),
            d = c.closest("form").find(c.data("counter")),
            h = c.data("limit");
        d.toggle(500 > h).text(h);
        a.on(c, "input paste", function() {
            var a = c.val(),
                b = h - e.countSymbols(a);
            0 > b && (c.val(e.limitSymbols(a, h)), b = 0);
            d.toggle(500 > b).text(b)
        });
        a.fire(c, "input")
    }
    a.on(d, "custom:ready lightbox:change", function(a) {
        b("textarea[data-counter][data-limit]", a.target).each(c)
    })
})(jQuery, document, Utils, Events);
Uploads = function(b, d) {
    var e = {
        file_size_error: "failure-too-big-text",
        identify_error: "failure-wrong-format-text"
    };
    return {
        onFailure: function(a, c) {
            if ("undefined" === typeof a.responseJSON) return d.error(b(c).data("failure-text"));
            b.each(e, function(e, g) {
                if (e === a.responseJSON.reason) return d.error(b(c).data(g)), !1
            })
        }
    }
}(jQuery, Flash);
UserAttachment = function(b, d, e, a, c) {
    function f(a) {
        b(a).find(".loaderUpload").hide();
        a.reset();
        d.release(a)
    }
    function g(a) {
        var d = b(this),
            e = b(this).parents(".uploadFormContainer").find(".avatar-thumb,.background-thumb"),
            g = d.data("params").thumbUrl,
            h = d.data("success-text"),
            d = d.data("success-toggle");
        a ? (e.slice(1).remove(), e.slice(0, 1).replaceWith(a)) : e.bg(g);
        c.notice(h);
        d && b(this).parents(".uploadFormContainer").find(d).toggle();
        f(this)
    }
    function h(a, c, d) {
        b(this).data("params", a);
        e.post(this).done(g).fail(function(a) {
            Uploads.onFailure(a,
                form);
            f(form)
        })
    }
    return {
        submitAction: function(c) {
            c.file.value && d.acquire(c) && (b(c).find(".loaderUpload").show(), a.upload(c).done(h).fail(function(a) {
                Uploads.onFailure(a, c);
                f(c)
            }))
        }
    }
}(jQuery, Mutex, Ajax, FormXHR2, Flash);
Viewport = function(b, d, e, a, c) {
    var f = b(d).width();
    a.on(d, "resize", c.debounce(function() {
        var c = b(d).width();
        c != f && a.fire(e, "custom:resize");
        f = c
    }, 100));
    return {}
}(jQuery, window, document, Events, Utils);
Actions("AvatarSelect", function(b, d, e) {
    return function(a) {
        var c = b(a).parent(),
            f = c.parent(),
            g = c.siblings(".avatar-thumb");
        e.acquire(f) && (g.first().addClass("loader"), d.put(a).done(function(a) {
            g.remove();
            c.replaceWith(a);
            e.release(f)
        }))
    }
}(jQuery, Ajax, Mutex));
Actions("AvatarDelete", function(b, d, e) {
    return function(a) {
        e.acquire(a) && (d._delete(a), b(a).parent().remove())
    }
}(jQuery, Ajax, Mutex));
Actions("CaptchaReload", Captcha.reloadAction);
Actions("FormXHRSubmit", FormXHR.submit);
Actions("ButtonSubmit", Button.submit);
Actions("ButtonSubmitOnce", Button.submitOnce);
Actions("ButtonPostOnce", Button.postOnce);
Actions("ButtonGetOnce", Button.getOnce);
Actions("CheckboxToggle", Checkbox.onToggle);
Actions("FlashShow", Flash.showAction);
Actions("FlashHide", Flash.hideAction);
Actions("BlockToggle", function(b) {
    return function(d) {
        d = b(d);
        var e = d.data("target"),
            a = d.data("class");
        a && d.toggleClass(a);
        b(e).stop(!1, !1).slideToggle(100)
    }
}(jQuery));
Actions("SelectRedirect", function(b) {
    return function(b) {
        Turbolinkz.visit(b.options[b.selectedIndex].value)
    }
}(window));
Actions("UserSearch", function(b, d, e) {
    function a(a) {
        var b = $(this.form).find(e.pagerSelector);
        e.$substituteItems(b, a);
        d.release(this)
    }
    return function(c) {
        d.acquire(c) && b.get(c).done(a)
    }
}(AjaxUI, Mutex, Pager));
Actions("DiscardBanner", function(b, d, e) {
    return function(a) {
        e.acquire(a) && (d.put(a), b(a).parent().remove())
    }
}(jQuery, Ajax, Mutex));
Actions("FavoriteToggle", function(b, d, e) {
    return function(a) {
        if (d.acquire(a)) {
            var c = $(a),
                f = c.data("url"),
                g = c.data("toggle-url"),
                h = c.data("error-message"),
                k = c.data("class");
            c.toggleClass(k);
            b.post(a).done(function() {
                c.data("url", g).data("toggle-url", f);
                d.release(a)
            }).fail(function() {
                c.toggleClass(k);
                e.error(h);
                d.release(a)
            })
        }
    }
}(Ajax, Mutex, Flash));
Actions("FeedToggle", function(b, d, e) {
    return function(a) {
        b.acquire(a);
        e.setCookie("skip_likes", "1", a.checked ? 31536E6 : -1, "/");
        d.reload()
    }
}(Mutex, Turbolinkz, Utils), {
    preventDefault: !1
});
Actions("FollowToggle", function(b, d) {
    function e(a) {
        var b = $(this),
            e = b.data("class"),
            g = 0 < a.url.indexOf("/follow");
        e ? b.data("url", a.url).text(a.text).toggleClass(e, g) : b.remove();
        d.release(this)
    }
    return function(a) {
        d.acquire(a) && b.post(a).done(e)
    }
}(Ajax, Mutex));
Actions("SubmitWithTimezone", function(b, d) {
    function e() {
        try {
            return Intl.DateTimeFormat().resolvedOptions().timeZone
        } catch (a) {}
    }
    return function(a) {
        b(a).find("[name*=gmt_offset]").val(-(new Date).getTimezoneOffset());
        b(a).find("[name*=tz_id]").val(e());
        d.submit(a)
    }
}(jQuery, FormXHR));
Actions("ServicePermit", function(b, d, e) {
    return function(a) {
        b(a).data("connected") ? e.toggle(a) : d.post(a)
    }
}(jQuery, Form, Checkbox));
Actions("HashtagDelete", function(b, d, e) {
    return function(a) {
        var c = b(a),
            f = c.siblings("input:first");
        e.fire(f, "focus");
        c.data("url") ? (c.addClass("loader"), d._delete(a, {
                data: {
                    hashtag: c.text()
                }
            }).done(function() {
                c.remove()
            })) : (c.remove(), e.fire(f, "input"))
    }
}(jQuery, Ajax, Events));
Actions("HashtagCreate", function(b, d) {
    var e = /#?(\S+) /g;
    return function(a) {
        var c = b(a),
            f = c.data("url"),
            g = c.val(),
            h;
        h = g.replace(e, function(e, g) {
            var h = b('<a href="" data-action="HashtagDelete" data-url=""></a>').text(g).data("url", f).insertBefore(c).before(" ").after(" ");
            f && (h.addClass("loader"), Ajax.post(a, {
                data: {
                    hashtag: g
                }
            }).done(function(a) {
                h.removeClass("loader");
                a && a.error && (h.remove(), d.error(a.error))
            }));
            return ""
        });
        g !== h && (c.val(h), Events.fire(c, "input"));
        if (!f) {
            var g = c.siblings("input[type=hidden]"),
                k = c.siblings("a");
            h = g.val();
            k = k.map(function() {
                return b(this).text()
            }).get().join(",");
            h !== k && (g.val(k), Events.fire(g, "input"))
        }
    }
}(jQuery, Flash));
Actions("PopupOpen", Lightbox.popupAction);
Actions("PopupClose", Lightbox.closeAction);
Actions("ImageOpen", Lightbox.imageAction);
Actions("ImagesOpen", Lightbox.imagesAction);
Actions("ImagesSelect", Lightbox.selectImageAction);
Actions("ImagesPrev", Lightbox.prevImageAction);
Actions("ImagesNext", Lightbox.nextImageAction);
Actions("GifToggle", Media.gifToggleAction);
Actions("IframeToggle", Media.iframeToggleAction);
Actions("ItemsMore", function(b, d, e) {
    function a(a, b, d) {
        e.$replaceWithItems(this, a)
    }
    return function(c) {
        b.acquire(c) && Ajax.get(c).done(a)
    }
}(Mutex, AjaxUI, Pager));
Actions("ItemsPoll", Poller.clickAction);
Actions("NotificationDelete", Notifications.deleteAction);
Actions("OnlineStatusShow", OnlineStatus.showAction);
Actions("OnlineStatusHide", OnlineStatus.hideAction);
Actions("UploadToggle", function(b) {
    return function(d) {
        d = b(d);
        var e = b(d.data("target"));
        d = b(".photopollWrap");
        e.stop(!1, !1).toggleClass("open").slideToggle(100).siblings().removeClass("open").slideUp(100);
        e = e.parent().find(".open").length ? "addClass" : "removeClass";
        d[e]("showUpload")
    }
}(jQuery));
Actions("PhotopollVote", function(b, d, e) {
    function a(a, b) {
        var d = $(b).closest(".streamItem"),
            e = d.find(".photopoll");
        d.find(".voteCount").text(a.total_votes);
        e.find(".vote, .owner-vote").toggle();
        e.find(".choices").each(function() {
            var b = $(this),
                d = b.data("option-id"),
                d = a.options[d],
                e = d.vote_percent,
                f = b.find(".rating");
            d.voted && b.addClass("voted");
            f.text(e + "%").animate({
                marginBottom: 3 * e + 100 + "px"
            }, 400)
        })
    }
    _CURRENT_VOTER_SELECTOR = ".current-voter";
    return function(c) {
        var f = $(c),
            g = f.closest(".choices"),
            h = f.closest(".streamItem"),
            k = h.find(".photopoll");
        d.acquire(h) && (k.addClass("loading"), b.post(c).done(function(b) {
            a(b, c);
            b = $(_CURRENT_VOTER_SELECTOR);
            b.length && (b.find(".votedOption[data-id!=" + g.data("option-id") + "]").remove(), e.$prependWithItems(".votes-container", b.html()), h.siblings(".votes-container").addClass("voted"))
        }).always(function() {
            k.removeClass("loading");
            d.release(h)
        }))
    }
}(Ajax, Mutex, Pager));
Actions("PhotopollDelete", function(b, d, e, a) {
    return function(c) {
        var f = $(c).data("confirm");
        if (!f || b.confirm(f)) {
            var g = $(c).data("after-url"),
                f = d._delete(c);
            g && f.done(function() {
                a.visit(g)
            });
            e.$removeClosestItem(c)
        }
    }
}(window, Ajax, Pager, Turbolinkz));
Actions("PhotopollSubmit", FormXHR.submit);
Actions("PolicyAccept", function(b, d, e, a) {
    return function(c) {
        c = b(c);
        if (e.acquire(c)) {
            c.closest(".policy-message").hide();
            var f = c.data("cookie");
            f ? a.setCookie(f, "1", 31536E6, "/") : d.put(c[0])
        }
    }
}(jQuery, Ajax, Mutex, Utils));
Actions("PopoverOpen", function(b, d, e, a) {
    function c() {
        _$active && (_$active.hide(), _$active = null, a.off(e, "click.popover"))
    }
    _$active = null;
    return function(f) {
        var g = b(f);
        f = g.next();
        c();
        var h = b(d).scrollTop(),
            g = g.offsetParent().offset().top,
            k = b("#topMenu").outerHeight(),
            l = f.outerHeight(),
            h = g - l > k + h;
        f.toggleClass("top", h).toggleClass("bottom", !h).show();
        _$active = f;
        a.on(e, "click.popover", c)
    }
}(jQuery, window, document, Events));
Actions("ProfileSettingSelect", function(b, d) {
    function e(a) {
        return 0 === a.indexOf("theme")
    }
    function a(a, d) {
        return b.grep(d.split(" "), e).join(" ")
    }
    return function(c) {
        c = b(c);
        var e = c.data("class"),
            g = c.data("value"),
            h = c.data("target");
        c.closest(".simpleFormItem").find("." + e).removeClass(e);
        c.addClass(e);
        b(h).val(g);
        "#user_theme_id" === h && (c = "theme" + g, b(d.body).removeClass(a).addClass(c))
    }
}(jQuery, document));
Actions("PromoClose", function(b, d) {
    return function(e) {
        var a = $(e);
        e = 1E3 * a.data("ttl");
        a = a.parent();
        d.setCookie("promo", "off", e, "/");
        a.hide();
        $(b.body).removeClass("appPromoBody")
    }
}(document, Utils));
Actions("PymkClose", Pymk.closeAction);
Actions("PymkClick", Pymk.clickAction);
Actions("AnswerLikeToggle", function(b, d, e, a) {
    return function(c) {
        if (d.acquire(c)) {
            var f = $(c),
                g = f.siblings(".counter"),
                h = f.hasClass("active") ? -1 : 1,
                k = parseInt(g.text()) + h;
            f.toggleClass("active", 1 === h);
            g.text(k).toggle(0 < k);
            e.update({
                likes: h
            });
            1 === h ? (b.post(c), f = $(".current-liker").html(), a.$prependWithItems(".likes-container", f)) : (b._delete(c), f = $(".current-liker div[data-login]").data("login"), f = $('.likes-container div[data-login="' + f + '"]'), a.$removeClosestItem(f));
            d.release(c)
        }
    }
}(Ajax, Mutex, ProfileTabCounters,
    Pager));
Actions("AnswerDelete", function(b, d, e, a, c) {
    function f(b) {
        var c = {
            answers: -1
        };
        if (b = $(b).closest(".streamItem").find(".likeButton .counter").text()) c.likes = -parseInt(b);
        a.update(c)
    }
    return function(a) {
        var h = $(a).data("confirm");
        if (!h || b.confirm(h)) {
            f(a);
            var k = $(a).data("after-url"),
                h = d._delete(a);
            k && h.done(function() {
                c.visit(k)
            });
            e.$removeClosestItem(a)
        }
    }
}(window, Ajax, Pager, ProfileTabCounters, Turbolinkz));
Actions("QuestionDelete", Questions.deleteAction);
Actions("QuestionsDelete", Questions.deleteAllAction);
Actions("QuestionRandom", Questions.randomAction);
Actions("MassAskSubmit", MassAsk.onSubmit);
Actions("MassAskToggleUser", MassAsk.onToggleUser);
Actions("MassAskToggleAnonymous", MassAsk.onToggleAnonymous, {
    preventDefault: !1
});
Actions("MassAskSearch", MassAsk.onSearch);
Actions("ReadMore", function() {
    return function(b) {
        b = $(b).closest(".streamItem");
        b.removeClass("shorten");
        b.children(".streamItemContent-embed").remove()
    }
}());
Actions("SaveQuestionAndPopupOpen", function(b, d, e) {
    return function(a) {
        var c = b(a).closest("form"),
            c = b.trim(c.find("[name*=question_text]").val());
        e.setCookie("question", c, c.length ? 6E5 : -1);
        d.popupAction(a)
    }
}(jQuery, Lightbox, Utils));
Actions("SignupCheck", function(b, d, e, a) {
    function c(a, c) {
        c ? b(a).parent().addClass("invalid-input").removeClass("valid-input") : b(a).parent().addClass("valid-input").removeClass("invalid-input")
    }
    function f(b) {
        b = b && b.error || null;
        c(this, b);
        a.message(b, null);
        g = null
    }
    var g;
    return function(a) {
        g && g.abort();
        d.isPresent(a.value) ? g = e.post(a).done(f) : c(a, !0)
    }
}(jQuery, Utils, Ajax, Flash));
Actions("SignupConfirmDob", function(b, d) {
    return d.debounce(function(d) {
        var a = b(d).closest("[data-confirm]");
        d = a.find("[name*=day]");
        var c = a.find("[name*=month]"),
            f = a.find("[name*=year]"),
            g = d.val(),
            h = c.val(),
            k = f.val();
        if (g && h && k) {
            var l = new Date,
                m = new Date(k, h - 1, g, 23, 59, 59);
            l.setFullYear(l.getFullYear() - 13);
            m < l || (a = a.data("confirm") + "\n" + g + "." + h + "." + k, window.confirm(a) || (d.val(""), c.val(""), f.val("")))
        }
    }, 300)
}(jQuery, Utils));
Actions("AttachmentSubmit", UserAttachment.submitAction);
Actions("PhotoSubmit", PhotoAnswer.submitAction);
Actions("PhotoRemove", PhotoAnswer.removeAction);
Actions("PhotoPollSubmit", PhotoPoll.submitAction);
Actions("UserBlockDelete", function(b, d, e, a, c) {
    function f(d) {
        a.$removeClosestItem(this);
        b("#blockCount").text(function(a, b) {
            return parseInt(b) - 1
        });
        c.message(d.error, d.notice);
        e.release(this)
    }
    return function(a) {
        e.acquire(a) && d._delete(a).done(f)
    }
}(jQuery, Ajax, Mutex, Pager, Flash));
Pages({
    notifications: ["index", "questions", "answers", "likes"]
}, Notifications.initClickActions);
Pages({
    profile: ["answers", "likes", "gifts", "answer"]
}, ProfileTabCounters.enable);
GTM.element("like-toggle", function(b) {
    b = b.hasClass("active");
    this.push({
        event: b ? "like" : "unlike"
    })
});
GTM.element("follow-toggle", function(b) {
    b = b.hasClass("icon-add-friend") || !b.data("class");
    this.push({
        event: b ? "follow" : "unfollow"
    })
});
GTM.element("report-user report-topic report-question", function(b, d, e, a) {
    this.push({
        event: d,
        "report-reason": e,
        "report-block": a
    })
});
GTM.element("popup", function(b, d, e, a, c) {
    this.push({
        event: e + "-" + d,
        "page.category": a,
        "page.type": c
    })
});
GTM.element("ask-question", function(b, d, e, a) {
    d = b.find("input[name*=anonymous]:checked").length;
    var c = "individual",
        f = 1;
    if ("mass" === e) {
        f = MassAsk.loginCount(b);
        if (0 === f) return;
        1 < f && (c = "mass")
    }
    this.push({
        event: a ? "reask-question" : "ask-question",
        "question-type": d ? "anonymous" : "identified",
        "question-target": c,
        "recipient-count": f
    });
    GTM.exec("mention-user", "question", b.find("textarea").val())
});
GTM.element("answer-question", function(b) {
    var d = this;
    d.push({
        event: "answer-question"
    });
    b.find("input[name*=sharing]:checked").each(function() {
        d.push({
            event: "share-answer",
            "sharing-option": $(this).val()
        })
    });
    GTM.exec("mention-user", "answer", b.find("textarea").val())
});
GTM.element("share-topic share-user", function(b, d) {
    var e = this;
    b.find("input[value=true]").each(function() {
        e.push({
            event: d,
            "sharing-option": $(this).attr("name")
        })
    })
});
GTM.routine("mention-user", function(b, d, e) {
    (b = (e.match(/\B(@[a-zA-Z][a-zA-Z0-9_]{2,}\b(?!@))/g) || []).length) && this.push({
        event: "mention-user",
        "entity-type": d,
        "mention-count": b
    })
});
GTM.routine("gif-load", function(b, d) {
    this.push({
        event: b,
        time: 1E4 < d ? "> 10000" : 5E3 < d ? "5001-10000" : 2E3 < d ? "2001-5000" : 500 < d ? "501-2000" : 200 < d ? "201-500" : 50 < d ? "51-200" : "cached"
    })
});
GTM.routine("gif-toggle", function(b, d, e, a) {
    this.push({
        event: d ? "gif-play" : "gif-stop",
        autoplay: e ? "on" : "off",
        actor: a ? "autoplay" : "user"
    })
});