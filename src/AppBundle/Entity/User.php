<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @UniqueEntity(fields={"email", "username"})
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser implements UserInterface
{

    use BaseEntity;

    const ROLE_USER = 'ROLE_USER';

    const GENDER_FEMALE = 1;
    const GENDER_MALE = 2;

    const SIDE_MENU_FOLLOWS_COUNT = 6;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="4")
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    protected $fullName;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    protected $biography;

    /**
     * @Assert\NotNull()
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    protected $bornAt;

    /**
     * @Assert\Choice(choices={User::GENDER_FEMALE, User::GENDER_MALE})
     * @ORM\Column(type="smallint", nullable=true)
     * @var int
     */
    protected $gender;

    /**
     * ORM\Column(type="string", nullable=true)
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Locale")
     * @ORM\JoinColumn(name="code", onDelete="SET NULL")
     * @var string
     */
    protected $localeCode;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\UserSocials", mappedBy="user", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     * @var UserSocials
     */
    protected $userSocials;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    protected $answersCount = 0;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    protected $likesCount = 0;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    protected $followersCount = 0;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    protected $followedCount = 0;

    /**
     * @var boolean
     */
    protected $followedByViewer = false;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Follow", mappedBy="followed", cascade={"remove"})
     */
    protected $followers;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Follow", mappedBy="follower", cascade={"remove"})
     */
    protected $followed;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Locale")
     * @ORM\JoinColumn(name="localeCode", onDelete="SET NULL")
     * @var Locale
     */
    protected $locale;

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->fullName ?? $this->username;
    }

    /**
     * @return string
     */
    public function getBiography()
    {
        return $this->biography;
    }

    /**
     * @param string $biography
     */
    public function setBiography(string $biography)
    {
        $this->biography = $biography;
    }

    /**
     * @param string $fullName
     */
    public function setFullName(string $fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * @return \DateTime
     */
    public function getBornAt()
    {
        return $this->bornAt;
    }

    /**
     * @param \DateTime $bornAt
     */
    public function setBornAt($bornAt)
    {
        $this->bornAt = $bornAt;
    }

    /**
     * @return int
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param int $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return string
     */
    public function getLocaleCode()
    {
        return $this->localeCode;
    }

    /**
     * @param string $localeCode
     */
    public function setLocaleCode($localeCode)
    {
        $this->localeCode = $localeCode;
    }

    /**
     * @return UserSocials
     */
    public function getUserSocials()
    {
        return $this->userSocials;
    }

    /**
     * @param UserSocials $userSocials
     * @return $this
     */
    public function setUserSocials(UserSocials $userSocials)
    {
        $this->userSocials = $userSocials;
        return $this;
    }

    /**
     * @return int
     */
    public function getAnswersCount()
    {
        return $this->answersCount;
    }

    /**
     * @param int $answersCount
     * @return $this
     */
    public function setAnswersCount(int $answersCount)
    {
        $this->answersCount = $answersCount;
        return $this;
    }

    /**
     * @return int
     */
    public function getLikesCount()
    {
        return $this->likesCount;
    }

    /**
     * @param int $likesCount
     * @return $this
     */
    public function setLikesCount(int $likesCount)
    {
        $this->likesCount = $likesCount;
        return $this;
    }

    /**
     * @return int
     */
    public function getFollowersCount()
    {
        return $this->followersCount;
    }

    /**
     * @param int $followersCount
     * @return $this
     */
    public function setFollowersCount(int $followersCount)
    {
        $this->followersCount = $followersCount;
        return $this;
    }

    /**
     * @return int
     */
    public function getFollowedCount()
    {
        return $this->followedCount;
    }

    /**
     * @param int $followedCount
     * @return $this
     */
    public function setFollowedCount(int $followedCount)
    {
        $this->followedCount = $followedCount;
        return $this;
    }

    /**
     * @return bool
     */
    public function isFollowedByViewer(): bool
    {
        return $this->followedByViewer;
    }

    /**
     * @param bool $followedByViewer
     * @return $this
     */
    public function setFollowedByViewer(bool $followedByViewer)
    {
        $this->followedByViewer = $followedByViewer;
        return $this;
    }

    /**
     * @return Locale
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param mixed $locale
     */
    public function setLocale(Locale $locale)
    {
        $this->locale = $locale;
    }

}
