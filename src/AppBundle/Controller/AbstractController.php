<?php

namespace AppBundle\Controller;

use AppBundle\Repository\{
    EventRepository, FollowRepository, QuestionLikeRepository, QuestionRepository, UserRepository, UserSocialsRepository
};
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

abstract class AbstractController extends Controller
{

    /**
     * @return EventRepository
     */
    protected function getEventRepository(): EventRepository
    {
        return $this->getDoctrine()->getRepository('AppBundle:Event');
    }

    /**
     * @return FollowRepository
     */
    protected function getFollowRepository(): FollowRepository
    {
        return $this->getDoctrine()->getRepository('AppBundle:Follow');
    }

    /**
     * @return QuestionLikeRepository
     */
    protected function getQuestionLikeRepository(): QuestionLikeRepository
    {
        return $this->getDoctrine()->getRepository('AppBundle:QuestionLike');
    }

    /**
     * @return QuestionRepository
     */
    protected function getQuestionRepository(): QuestionRepository
    {
        return $this->getDoctrine()->getRepository('AppBundle:Question');
    }

    /**
     * @return UserRepository
     */
    protected function getUserRepository(): UserRepository
    {
        return $this->getDoctrine()->getRepository('AppBundle:User');
    }

    /**
     * @return UserSocialsRepository
     */
    protected function getUserSocialsRepository(): UserSocialsRepository
    {
        return $this->getDoctrine()->getRepository('AppBundle:UserSocials');
    }

    /**
     * @param mixed $data
     * @param int $status
     * @param array $headers
     * @param array $context
     * @return JsonResponse
     */
    protected function json($data, $status = 200, $headers = [], $context = []): JsonResponse
    {
        return (new JsonResponse())->setJson($this->get('serializer')->serialize($data, 'json'));
    }

}
