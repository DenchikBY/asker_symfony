<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Event;
use AppBundle\Entity\User;

class EventRepository extends AbstractRepository
{

    /**
     * @param User $user
     * @param int $minId
     * @param int $maxId
     * @param int $limit
     * @param int $type
     * @return Event[]|null
     */
    public function getFeed(User $user, int $minId = 0, int $maxId = 0, int $limit = 12, int $type = Event::TYPE_ANSWER)
    {
        $query = $this->createQueryBuilder('e')
            ->join('e.question', 'q')
            ->join('e.eventUser', 'event_user')
            ->join('e.questionReceiverUser', 'question_receiver_user')
            ->where('e.eventUser in (:user)')
            ->orderBy('e.id', 'desc')
            ->setMaxResults($limit)
            ->setParameter('user', $user);
        if ($type == Event::TYPE_ANSWER) $query->andWhere("e.type = $type");
        if ($minId > 0 && $maxId > 0) $query->andWhere("e.id > $minId and e.id < $maxId");
        else if ($minId > 0) $query->andWhere("e.id > $minId");
        else if ($maxId > 0) $query->andWhere("e.id < $maxId");
        return $query->getQuery()->getResult();
    }

}
