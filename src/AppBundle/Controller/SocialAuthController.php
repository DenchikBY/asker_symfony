<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class SocialAuthController extends AbstractController
{

    /**
     * @Route("/disconnect/{owner}", name="site_social_disconnect")
     * @param string $owner
     * @return Response
     */
    public function disconnectAction(string $owner): Response
    {
        $user = $this->getUser();
        $userSocials = $user->getUserSocials();
        $userSocials->{'set' . ucfirst($owner) . 'Id'}(null);
        $userSocials->{'set' . ucfirst($owner) . 'AccessToken'}(null);
        $this->getUserRepository()->update($userSocials);
        return $this->redirectToRoute('site_account_settings');
    }

}
